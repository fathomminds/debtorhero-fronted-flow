<?php
ob_start();
require 'Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$class = "desktop";
if($deviceType=='tablet'){
	$class = "tablet";
} else if($deviceType=='phone'){
    $class = "phone";
}
?>
<html>
<head>
    <title>DebtorHero - Receive better settlement terms for your debts</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- FLOW CORE SCRIPTS -->
    <script type="text/javascript" src="assets/scripts/vendor/flow/FlowCore.js"></script>
    <script type="text/javascript" src="assets/scripts/vendor/flow/FlowBlock.js"></script>
    <script type="text/javascript" src="assets/scripts/vendor/flow/FlowPacket.js"></script>
    <script type="text/javascript" src="assets/scripts/vendor/flow/FlowPort.js"></script>
    <script type="text/javascript" src="assets/scripts/vendor/flow/FlowDispatcher.js"></script>

    <!-- VENDOR SCRIPTS -->
    <script type="text/javascript" src="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/scripts/vendor/jquery/jquery-3.1.0.min.js"></script>

    <!-- STYLESHEETS -->
    <link rel="stylesheet" href="assets/css/style.css" media="screen"/>

    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="57x57" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/favicon-16x16.png">

    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Hind' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="assets/scripts/app.js"></script>
    <script type="text/javascript">var CurrentDevice = '<?=$class?>';</script>
</head>
<body class="<?=$class?>" id="application">
<div id="header">
    <div class="homelink" style="padding: 1rem; float: left; height: 3rem; width: 35%;">
        <a href="#about" id="home-mnu">Debtor Hero</a>
    </div>
    <div class="menulinks" style="padding: 1rem; padding-top: 1.1rem; float: right; height: 3rem; width: 45%; text-align: right;">
        <a href="#about" id="about-mnu">ABOUT</a> | <a href="#careers" class="careers-mnu" id="join-btn-1">JOIN</a> | <a href="#" id="signin-btn">SIGN IN</a>
    </div>
</div>
<div id="video-wrapper">
    <video autoplay loop id="bgvid">
        <source src="assets/video/happyfamily_1.mp4" type="video/mp4">
    </video>
    <div class="overlay"></div>
    <div class="intro-wrapper">
        <div class="intro">
            <span>Better settlement terms for your debts</span><br /><br />
            <a href="#about" class="intro-btn" id="intro-btn-1">Read More</a>
        </div>
    </div>
</div>
<div id="video-fallback-wrapper">
    <div class="overlay"></div>
    <div class="intro-wrapper">
        <div class="intro">
            <span>Better settlement terms for your debts</span><br /><br />
            <a href="#about" class="intro-btn" id="intro-btn-2">Read More</a>
        </div>
    </div>
</div>
<div id="content">
    <div class="block heading" id="about">
        <h1>DEBTOR HERO</h1>
    </div>
    <div class="subcontent">
        We all have debts. For many of us, repaying can be difficult and stressful. Ever increasing interest, penalty charges, legal charges.
        <br /><br />
        Trying to solve these problems on your own, is hard. The creditors smile when they sell you a product, then humiliate and chase you when you can’t pay.
        <br /><br />
        At Debtor Hero, we have assembled a team of international experts who understand exactly how to negotiate with the Creditor.
        <br /><br />
        Now we offer this unique expertise to you.<br />
        <button class="careers-mnu round-btn" id="join-btn-2" style="width: 250px; height: 60px; padding-top: 1rem; color: #555; border-color: #555; margin-top: 2rem; margin-bottom: 2rem;">
            Join the Community
        </button>
        <div class="clr"></div>
        <div class="left-col">
            <h2>Strength in numbers</h2>
            <p>
                We are a community of people all with the same issue, unpaid debts. As the community grows the negotiation strength increases allowing our team to negotiate significant discounts on your behalf.
            </p>
        </div>
        <div class="right-col">
            <h2>Better settlement terms</h2>
            <p>
                The combination of Debtor Hero’s teams expertise in debt negotiation and representing a large community of debtors we are able to provide the strength and knowledge to negotiate significantly better payment terms towards creditors.  We understand what is acceptable for a creditor and what they will settle for.
            </p>
        </div>
        <div id="bounce" class="bounce"></div>
        <div class="clr"></div>
    </div>
    <div class="full-row" style="margin-top: 5vw; margin-bottom: 3vw; font-size: 0;">
        <div class="third-col-left">
            <div class="rounded-image cognitive-computing-bg"></div>
            <h2>Community</h2>
            <p>
                Membership of the Debtor Hero community allows you to benefit from our professionals experience, receive personalized advice from our team of experts and settle your debts in a positive way.
            </p>
        </div>
        <div class="third-col-middle">
            <div class="rounded-image machine-learning-bg"></div>
            <h2>Discount</h2>
            <p>
                Strength in numbers, creditors want to settle, but have little time or motivation to negotiate with a single person.  Representing the community against one creditor can achieve both settlement and a significant discount.
            </p>
        </div>
        <div class="third-col-right">
            <div class="rounded-image predictive-api-bg"></div>
            <h2>My credit rating</h2>
            <p>
                Settle as a member of the community and we will also negotiate to have the settlement entered into the bank and national debtors list.  For the future this will mean you can borrow again without the penalty of bad debt.
            </p>
        </div>
        <div class="clr"></div>
    </div>
    <div class="full-row project-row" style="background: rgb(27,68,82);">
        <div class="left-full-col project-col-text project-col-float-left autoHeight">
            <h1>How much of a discount will I be able to get?</h1>
            <p style="color: #eee;">
                Depends on the community strength against each creditor. Anything from 10-50%.
            </p>
            <!--
            <div class="project-button">
                <button class="round-btn careers-mnu" id="join-btn-1" style="color: #eee; border-color: #eee;">Join</button>
            </div>
            -->
        </div>
        <div class="right-full-col project-col-text project-col-abs-right autoHeight" style="background: #eee; color: rgb(27,68,82);">
            <h1 style="color: rgb(27,68,82);">What kind of debts can I enter?</h1>
            <p>
                Any debt over 6 months old from last payment.
            </p>
            <!--
            <div class="project-button">
                <button class="round-btn careers-mnu" id="join-btn-2" style="color: rgb(27,68,82); border-color: rgb(27,68,82);">Join</button>
            </div>
            -->
        </div>
    </div>
    <div class="full-row project-row phone-dark" style="background: #eee;">
        <div class="left-full-col project-col-text project-col-float-left phone-dark autoHeight">
            <h1 class="phone-dark" style="color: rgb(27,68,82);">Why will the creditor negotiate with DebtorHero and not with me?</h1>
            <p class="phone-dark" style="color: rgb(27,68,82);">
                Negotiating as a representative of thousands gives DebtorHero stronger leverage towards the creditor.
            </p>
            <!--
            <div class="project-button phone-dark">
                <button class="round-btn careers-mnu phone-dark" id="join-btn-3" style="color: rgb(27,68,82); border-color: rgb(27,68,82);">Join</button>
            </div>
            -->
        </div>
        <div class="right-full-col project-col-text project-col-abs-right phone-light autoHeight" style="background: rgb(27,68,82); color: #eee;">
            <h1 class="phone-light" style="color: #eee;">Question 4?</h1>
            <p class="phone-light" style="">
                Vivamus pretium vel magna non ultricies. Praesent pulvinar eros id felis interdum, facilisis tempus ante pretium.
            </p>
            <!--
            <div class="project-button phone-light">
                <button class="round-btn careers-mnu phone-light" style="color: #eee; border-color: #eee;">Join</button>
            </div>
            -->
        </div>
    </div>
</div>
<div id="careers">
    <div class="block heading" id="about">
        <h1>JOIN THE COMMUNITY</h1>
    </div>
    <div class="career-subcontent">
        As soon as you join, you’ll be notified and receive a confirmation email.
        <br />
        <br />
        <div class="clr"></div>
    </div>
    <div class="career-form">
        <div id="signupform">
            <form id="career-form-wrapper" onsubmit="return false;">
                <ul class="input-list style-1 clearfix" id="signInTab1">
                    <li>
                        <input type="file" name="file" id="file" class="inputfile" data-default-caption="Click here to upload your CV" data-multiple-caption="{count} files selected" />
                        <label for="file"><img src="assets/images/file_upload_btn.png" style="vertical-align: middle;" /> <span id="fileinput-text">Click here to upload your demand of payment</span></label>
                    </li>
                    <li>
                        <input id="career-fullname" type="text" placeholder="Full name" required="required" />
                    </li>
                    <li>
                        <input id="career-email" type="email" placeholder="Email address" required="required" />
                    </li>
                    <li>
                        <input id="career-phone" type="text" placeholder="Phone number" />
                    </li>
                    <li>
                        <input id="connection_token" type="hidden" name="connection_token" />
                    </li>
                    <li>
                        <input id="accept-terms" type="checkbox" />
                        <label for="accept-terms"><span></span>I confirm I read the <a style="color: #00719b;" id="terms-open-btn" href="#">Terms and Conditions</a></label>
                    </li>
                </ul>
                <ul>
                    <li style="text-align: center;">
                        <a id="apply-btn" class="round-btn" style="display: inline-block; width: 200px; padding-top: 1rem; color: #555; border-color: #555; margin-top: 2rem;">
                            <span id="ripple" style="display: none; margin-top: -10px;"><img src="assets/images/ripple.gif" /></span>
                            <span id="signuptext">Join</span>
                        </a>
                    </li>
                </ul>
            </form>
        </div>
        <div id="passwordform">
            <form id="password-form-wrapper" onsubmit="return false;">
                <input id="uid" type="hidden" />
                <input id="token" type="hidden" />
                <ul class="input-list style-1 clearfix">
                    <li>
                        <input id="password" type="password" placeholder="Password" />
                        <div class="hint"><i>The password must contain: small letter, uppercase letter, number, special character and must be at least 8 characters long.</i></div>
                    </li>
                    <li>
                        <input id="confirm-password" type="password" placeholder="Confirm password" required="required" />
                    </li>
                </ul>
                <ul>
                    <li style="text-align: center;">
                        <a id="backToTab1-btn" class="round-btn" style="background:#666; display: inline-block; width: auto; padding-top: 1rem; color: #fff; border-color: #555; margin-top: 2rem; margin-right: 40px;">
                            <span id="ripple2" style="display: none; margin-top: -10px;"><img src="assets/images/ripple.gif" /></span>
                            <span id="setpasswordtext">Back</span>
                        </a><a id="setpassword-btn" class="round-btn" style="display: inline-block; width: 200px; padding-top: 1rem; color: #555; border-color: #555; margin-top: 2rem;">
                            <span id="ripple2" style="display: none; margin-top: -10px;"><img src="assets/images/ripple.gif" /></span>
                            <span id="setpasswordtext">Set Password</span>
                        </a>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</div>
<div id="footer">
    <div class="footer-left">
        <p class="footer-head">
            <a href="mailto:no-spam-please" class="contact-mnu">Contact</a>
            | <a id="terms-btn" href="#terms" class="terms-mnu">Terms and Conditions</a></p>
        <p class="footer-text">Copyright &copy; 2016 DebtorHero</p>
    </div>
    <div class="footer-middle">
        <p class="footer-head" style="padding-left: 10vw;"></p>
        <p class="footer-text" style="padding-left: 10vw;"></p>
    </div>
    <div class="footer-right">
        <p class="footer-head" style="padding-left: 10vw;">1234 Budapest, Hungary</p>
        <p class="footer-text" style="padding-left: 10vw;">123 Main street</p>
    </div>
</div>
<div id="blackOverlay"></div>
<div id="notification"></div>
<div id="signin-modal" class="modal-window">
    <div class="modal-content">
        <div class="modal-header"><h1>Sign in</h1></div>
        <div class="modal-text">
            <input id="signin-email" type="email" placeholder="Email" required="required" />
            <div class="s1rem"></div>
            <input id="signin-password" type="password" placeholder="Password" required="required" />
            <div class="s1rem"></div>
            <a hreF="#" id="password-reset">Forgotten password</a>
        </div>
        <div class="modal-controls">
            <button id="signin-submit-btn" class="submit">Sign in</button>
            <button id="signin-close-btn">Cancel</button>
        </div>
    </div>
    <!--
    <div id="signin-controls" style="display: table-cell; vertical-align: middle; text-align: center; height: 100%;">
        <div id="signin-controls-box">
            <form onsubmit="$('#signin-submit-btn').click(); return false;">
                <input id="signin-email" type="email" class="signin-input" placeholder="Email" /><br />
                <input id="signin-password" type="password" class="signin-input" placeholder="Password" /><br />
                <a id="signin-submit-btn">
                    Sign in
                </a>
            </form>
        </div>
    </div>
    -->
</div>

<div id="terms-modal" class="modal-window">
    <div class="modal-content">
        <div class="modal-header"><h1>TERMS AND CONDITIONS</h1></div>
        <div class="modal-text" id="terms-text">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris posuere sem a finibus ultricies. Sed elementum hendrerit magna quis dignissim. Sed vitae libero id erat pellentesque dictum. In hac habitasse platea dictumst. Nunc rutrum mollis sagittis. Phasellus a pellentesque enim. In hac habitasse platea dictumst. Proin viverra metus quis pretium cursus.</p>
            <p>Integer quis blandit quam, et malesuada ante. Duis ut porta lacus, quis tincidunt quam. Duis vitae venenatis magna, gravida congue purus. Donec vel pretium felis. Vivamus vestibulum consequat sapien, et egestas est scelerisque sed. Morbi tempus mollis venenatis. Maecenas aliquet lectus vitae faucibus eleifend. Nunc maximus nisl ut augue luctus efficitur. Praesent at tempor metus.</p>
            <p>Quisque justo quam, pretium nec leo vel, ultricies consequat mauris. In eget felis id ipsum feugiat mattis. Donec tristique eleifend nibh eget placerat. Nulla facilisi. Curabitur semper magna id mi porta blandit. Etiam consequat nunc id scelerisque efficitur. Duis a hendrerit metus. Vestibulum nunc lacus, pulvinar in vestibulum ac, sagittis sed leo.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris posuere sem a finibus ultricies. Sed elementum hendrerit magna quis dignissim. Sed vitae libero id erat pellentesque dictum. In hac habitasse platea dictumst. Nunc rutrum mollis sagittis. Phasellus a pellentesque enim. In hac habitasse platea dictumst. Proin viverra metus quis pretium cursus.</p>
            <p>Integer quis blandit quam, et malesuada ante. Duis ut porta lacus, quis tincidunt quam. Duis vitae venenatis magna, gravida congue purus. Donec vel pretium felis. Vivamus vestibulum consequat sapien, et egestas est scelerisque sed. Morbi tempus mollis venenatis. Maecenas aliquet lectus vitae faucibus eleifend. Nunc maximus nisl ut augue luctus efficitur. Praesent at tempor metus.</p>
            <p>Quisque justo quam, pretium nec leo vel, ultricies consequat mauris. In eget felis id ipsum feugiat mattis. Donec tristique eleifend nibh eget placerat. Nulla facilisi. Curabitur semper magna id mi porta blandit. Etiam consequat nunc id scelerisque efficitur. Duis a hendrerit metus. Vestibulum nunc lacus, pulvinar in vestibulum ac, sagittis sed leo.</p>
        </div>
        <div class="modal-controls">
            <button id="terms-accept-btn">Accept terms and conditions</button>
            <button id="terms-close-btn">Cancel</button>
        </div>
    </div>
</div>
<div id="loading"></div>
<script type="text/javascript" src="assets/scripts/flow/index/index.js"></script>
<script type="text/javascript" src="assets/scripts/flow/index/joinForm.js"></script>
<script type="text/javascript" src="assets/scripts/flow/index/validateFullname.js"></script>
<script type="text/javascript" src="assets/scripts/flow/index/validateEmail.js"></script>
<script type="text/javascript" src="assets/scripts/flow/index/validatePhone.js"></script>
<script type="text/javascript" src="assets/scripts/flow/index/validateTerms.js"></script>
<script type="text/javascript" src="assets/scripts/flow/index/signIn.js"></script>
<script type="text/javascript" src="assets/scripts/flow/notification.js"></script>
</body>
</html>
