Flows.ValidatePhoneFlow = {};
Flows.ValidatePhoneFlow.Validate = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var _data = _this.getIncomingPort('in').getPacket().getData();
        //var re = /^(\()?\d{3}(\))?( |\s)?\d{2}( |\s)\d{3}?( |\s)\d{4}$/;

        if(_data=='' || !_data || _data==null){
            var Error = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                ['Phone can not be empty!'],
                null
            );
            _this.setOutgoingPort('error', Error);
            _this.sendPort(Dispatchers.ValidatePhone, 'error');
        } /*else if(!re.test(_data)){
            var Error = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                ['Invalid phone number format!'],
                null
            );
            _this.setOutgoingPort('error', Error);
            _this.sendPort(Dispatchers.ValidatePhone, 'error');
        } */else {
            var Success = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                ['OK'],
                null
            );
            _this.setOutgoingPort('out', Success);
            _this.sendPort(Dispatchers.ValidatePhone, 'out');
        }

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidatePhoneFlow.Validate, FathomMinds.FlowCore.FlowBlock);
Flows.ValidatePhoneFlow.Validate.prototype._incomingPortNames = ['in'];
Flows.ValidatePhoneFlow.Validate.prototype._outgoingPortNames = ['out', 'error'];

Flows.ValidatePhoneFlow.Error = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var _data = _this.getIncomingPort('in').getPacket().getMessages()[0];
        console.error(_data);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidatePhoneFlow.Error, FathomMinds.FlowCore.FlowBlock);
Flows.ValidatePhoneFlow.Error.prototype._incomingPortNames = ['in'];
Flows.ValidatePhoneFlow.Error.prototype._outgoingPortNames = [];

Flows.ValidatePhoneFlow.Success = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var _data = _this.getIncomingPort('in').getPacket().getMessages()[0];
        console.log(_data);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidatePhoneFlow.Success, FathomMinds.FlowCore.FlowBlock);
Flows.ValidatePhoneFlow.Success.prototype._incomingPortNames = ['in'];
Flows.ValidatePhoneFlow.Success.prototype._outgoingPortNames = [];

Dispatchers.ValidatePhone = new FathomMinds.FlowCore.FlowDispatcher({
    'blocks':{
        'Phone-01-Validate': Flows.ValidatePhoneFlow.Validate,
        'Phone-02-01-Error': Flows.ValidatePhoneFlow.Error,
        'Phone-02-02-Success': Flows.ValidatePhoneFlow.Success
    },
    'connections': [
        ['Phone-01-Validate', 'error', 'Phone-02-01-Error', 'in'],
        ['Phone-01-Validate', 'out', 'Phone-02-02-Success', 'in']
    ]
});
