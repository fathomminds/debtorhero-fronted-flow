Flows.JoinFormFlow = {};
Flows.JoinFormFlow.CollectData = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.clear();
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var packet = _this.getIncomingPort('in').getPacket();

        if(packet.getType() == 'SUCCESS' && packet.getMessages()[0]=='back'){
            $('#signupform').show();
            $('#passwordform').hide();
        } else {

            var fullName = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['Sending fullName for validation'],
                $('#career-fullname').val()
            );

            _this.setOutgoingPort('out-fullName', fullName);
            _this.sendPort(Dispatchers.JoinFlow, 'out-fullName');

            var email = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['Sending email for validation'],
                $('#career-email').val()
            );

            _this.setOutgoingPort('out-email', email);
            _this.sendPort(Dispatchers.JoinFlow, 'out-email');

            var terms = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['send terms check for validation'],
                $('#accept-terms')[0].checked
            );
            _this.setOutgoingPort('out-terms', terms);
            _this.sendPort(Dispatchers.JoinFlow, 'out-terms');
        }

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.JoinFormFlow.CollectData, FathomMinds.FlowCore.FlowBlock);
Flows.JoinFormFlow.CollectData.prototype._incomingPortNames = ['in'];
Flows.JoinFormFlow.CollectData.prototype._outgoingPortNames = ['out-fullName', 'out-email', 'out-terms'];

Flows.JoinFormFlow.ValidateFullName = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        //Check fullName
        var fullName = _this.getIncomingPort('in').getPacket();
        Dispatchers.ValidateFullName = new FathomMinds.FlowCore.FlowDispatcher({
            'blocks':{
                'FullName-01-Validate': Flows.ValidateFullNameFlow.Validate,
                'FullName-02-01-Error': Flows.ValidateFullNameFlow.Error,
                'FullName-02-02-Success': Flows.ValidateFullNameFlow.Success
            },
            'connections': [
                ['FullName-01-Validate', 'error', 'FullName-02-01-Error', 'in'],
                ['FullName-01-Validate', 'out', 'FullName-02-02-Success', 'in']
            ]
        });
        Dispatchers.ValidateFullName.onOutput = function(){
            _this.setOutgoingPort('out', Dispatchers.ValidateFullName.getOutput());
            _this.sendPort(Dispatchers.JoinFlow, 'out');
        };
        Dispatchers.ValidateFullName.run([['FullName-01-Validate', 'in', fullName]]);


        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.JoinFormFlow.ValidateFullName, FathomMinds.FlowCore.FlowBlock);
Flows.JoinFormFlow.ValidateFullName.prototype._incomingPortNames = ['in'];
Flows.JoinFormFlow.ValidateFullName.prototype._outgoingPortNames = ['out'];

Flows.JoinFormFlow.ValidateEmail = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        //Check email
        var email = _this.getIncomingPort('in').getPacket();
        Dispatchers.ValidateEmail = new FathomMinds.FlowCore.FlowDispatcher({
            'blocks':{
                'Email-01-Validate': Flows.ValidateEmailFlow.Validate,
                'Email-02-01-Error': Flows.ValidateEmailFlow.Error,
                'Email-02-02-Success': Flows.ValidateEmailFlow.Success
            },
            'connections': [
                ['Email-01-Validate', 'error', 'Email-02-01-Error', 'in'],
                ['Email-01-Validate', 'out', 'Email-02-02-Success', 'in']
            ]
        });
        Dispatchers.ValidateEmail.onOutput = function(){
            _this.setOutgoingPort('out', Dispatchers.ValidateEmail.getOutput());
            _this.sendPort(Dispatchers.JoinFlow, 'out');
        };
        Dispatchers.ValidateEmail.run([['Email-01-Validate', 'in', email]]);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.JoinFormFlow.ValidateEmail, FathomMinds.FlowCore.FlowBlock);
Flows.JoinFormFlow.ValidateEmail.prototype._incomingPortNames = ['in'];
Flows.JoinFormFlow.ValidateEmail.prototype._outgoingPortNames = ['out'];

Flows.JoinFormFlow.ValidateTerms = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        // Check terms
        var checkTermsPacket = _this.getIncomingPort('in').getPacket();

        Dispatchers.ValidateTerms = new FathomMinds.FlowCore.FlowDispatcher({
            'blocks':{
                'Terms-01-Validate': Flows.ValidateTermsFlow.Validate,
                'Terms-02-01-Error': Flows.ValidateTermsFlow.Error,
                'Terms-02-02-Success': Flows.ValidateTermsFlow.Success
            },
            'connections': [
                ['Terms-01-Validate', 'error', 'Terms-02-01-Error', 'in'],
                ['Terms-01-Validate', 'out', 'Terms-02-02-Success', 'in']
            ]
        });
        Dispatchers.ValidateTerms.onOutput = function(){
            _this.setOutgoingPort('out', Dispatchers.ValidateTerms.getOutput());
            _this.sendPort(Dispatchers.JoinFlow, 'out');
        };
        Dispatchers.ValidateTerms.run([['Terms-01-Validate', 'in', checkTermsPacket]]);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.JoinFormFlow.ValidateTerms, FathomMinds.FlowCore.FlowBlock);
Flows.JoinFormFlow.ValidateTerms.prototype._incomingPortNames = ['in'];
Flows.JoinFormFlow.ValidateTerms.prototype._outgoingPortNames = ['out'];


Flows.JoinFormFlow.SetPassword = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        $('#signupform').hide();
        $('#passwordform').show();

        var backFunction = function(){
            var packet = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['back'],
                null
            );
            _this.setOutgoingPort('back', packet);
            _this.sendPort(Dispatchers.JoinFlow, 'back');
        };

        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('password'), 'focus', function(){ $('#password').removeClass('input-error') } );

        Dispatchers.JoinFlow.registerEvent(
            _this.getId(),
            document.getElementById('backToTab1-btn'),
            'click',
            backFunction
        );

        var checkPasswordStrength = function(){

            var hasLowerCase = function(str){
                return (/[a-z]/.test(str));
            };
            var hasUpperCase = function(str){
                return (/[A-Z]/.test(str));
            };
            var hasDigits = function(str){
                return (/\d/.test(str));
            };
            var hasSpecChars = function(str){
                return (/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str));
            };
            var longerThan = function(str, limit){
                if(str.length >= limit) return true;
                return false;
            };

            var totalScore = 0;
            if(longerThan($('#password').val(), 8)){
                totalScore += 20;
            }
            if( hasLowerCase($('#password').val()) ){
                totalScore += 20;
            }
            if( hasUpperCase($('#password').val()) ){
                totalScore += 20;
            }
            if( hasDigits($('#password').val()) ){
                totalScore += 20;
            }
            if( hasSpecChars($('#password').val()) ){
                totalScore += 20;
            }
            if(totalScore == 20) $('#password').removeClass('pswd-strong').removeClass('pswd-weak').removeClass('pswd-medium').addClass('pswd-very-weak');
            if(totalScore == 40) $('#password').removeClass('pswd-very-weak').removeClass('pswd-strong').removeClass('pswd-medium').addClass('pswd-weak');
            if(totalScore == 60) $('#password').removeClass('pswd-very-weak').removeClass('pswd-weak').removeClass('pswd-strong').addClass('pswd-medium');
            if(totalScore == 100) $('#password').removeClass('pswd-very-weak').removeClass('pswd-weak').removeClass('pswd-medium').addClass('pswd-strong');
            return totalScore;
        };

        Dispatchers.JoinFlow.registerEvent(
            _this.getId(),
            document.getElementById('password'),
            'keyup',
            checkPasswordStrength
        );

        var nextFunction = function(){
            if(checkPasswordStrength()<100){
                var errorPacket = new FathomMinds.FlowCore.FlowPacket(
                    FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                    ['The typed password is not complex enough!'],
                    null
                );
                // NOTIFICATION
                Dispatchers.Notification = new FathomMinds.FlowCore.FlowDispatcher({
                    'blocks':{
                        'Notificaion-Show': Flows.NotificationFlow.Show
                    },
                    'connections': []
                });

                Dispatchers.Notification.run([['Notificaion-Show', 'in', errorPacket]]);

            } else if($('#password').val()!=$('#confirm-password').val()){
                var errorPacket = new FathomMinds.FlowCore.FlowPacket(
                    FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                    ['The two password must be the same!'],
                    null
                );
                // NOTIFICATION
                Dispatchers.Notification = new FathomMinds.FlowCore.FlowDispatcher({
                    'blocks':{
                        'Notificaion-Show': Flows.NotificationFlow.Show
                    },
                    'connections': []
                });

                Dispatchers.Notification.run([['Notificaion-Show', 'in', errorPacket]]);
            } else {
                var _legacy_data = _this.getIncomingPort('in').getPacket();
                var data = _legacy_data.getData();
                data.password = $('#password').val();
                _legacy_data.setData(data);

                _this.setOutgoingPort('out', _legacy_data);
                _this.sendPort(Dispatchers.JoinFlow, 'out');
            }
        };

        Dispatchers.JoinFlow.registerEvent(
            _this.getId(),
            document.getElementById('setpassword-btn'),
            'click',
            nextFunction
        );

        var checkKeyUpSetPassword = function(e){ if(e.keyCode==13){ nextFunction(); } };

        Dispatchers.JoinFlow.registerEvent( _this.getId(), document.getElementById('password'), 'keyup', checkKeyUpSetPassword );
        Dispatchers.JoinFlow.registerEvent( _this.getId(), document.getElementById('confirm-password'), 'keyup', checkKeyUpSetPassword );

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.JoinFormFlow.SetPassword, FathomMinds.FlowCore.FlowBlock);
Flows.JoinFormFlow.SetPassword.prototype._incomingPortNames = ['in'];
Flows.JoinFormFlow.SetPassword.prototype._outgoingPortNames = ['out', 'error', 'back'];

Flows.JoinFormFlow.CheckResults = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var results = new FathomMinds.FlowCore.FlowPacket(
            FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
            ['init tab 2 - set password'],
            null
        );

        var data = {};
        for(var i in _this.getIncomingPortNames()){
            if(_this.getIncomingPort(_this.getIncomingPortNames()[i]).getPacket().getType() == 'ERROR'){
                _this.setOutgoingPort('error' , _this.getIncomingPort(_this.getIncomingPortNames()[i]).getPacket());
                _this.sendPort(Dispatchers.JoinFlow, 'error');
                return false;
            } else {
                //console.log(_this.getIncomingPort(_this.getIncomingPortNames()[i]).getPacket());
                for(var key in _this.getIncomingPort(_this.getIncomingPortNames()[i]).getPacket().getData()){
                    data[key] = _this.getIncomingPort(_this.getIncomingPortNames()[i]).getPacket().getData()[key];
                }
            }
        }

        data.phone = $('#career-phone').val();;
        results.setData(data);
        //console.log(results);
        _this.setOutgoingPort('out', results);
        _this.sendPort(Dispatchers.JoinFlow, 'out');

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.JoinFormFlow.CheckResults, FathomMinds.FlowCore.FlowBlock);
Flows.JoinFormFlow.CheckResults.prototype._incomingPortNames = ['in-fullName', 'in-email', 'in-terms'];
Flows.JoinFormFlow.CheckResults.prototype._outgoingPortNames = ['out', 'error'];

Flows.JoinFormFlow.CheckResults.prototype.isBlockReady = function(incomingPorts){
    if(incomingPorts['in-fullName'].getPacket() && incomingPorts['in-email'].getPacket() && incomingPorts['in-terms'].getPacket()){
        return true
    }
    return false;
};

Flows.JoinFormFlow.Error = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var result = _this.getIncomingPort('in').getPacket();
        //console.error(result.getMessages()[0]);

        // NOTIFICATION
        Dispatchers.Notification = new FathomMinds.FlowCore.FlowDispatcher({
            'blocks':{
                'Notificaion-Show': Flows.NotificationFlow.Show
            },
            'connections': []
        });

        Dispatchers.Notification.run([['Notificaion-Show', 'in', result]]);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.JoinFormFlow.Error, FathomMinds.FlowCore.FlowBlock);
Flows.JoinFormFlow.Error.prototype._incomingPortNames = ['in'];
Flows.JoinFormFlow.Error.prototype._outgoingPortNames = [];

Flows.JoinFormFlow.ProcessForm = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var packet = _this.getIncomingPort('in').getPacket().jsonSerialize();
        var file = $('#file')[0].files[0];
        var formData = new FormData();
        formData.append("file", file);
        formData.append("data", packet);
        /*
        formData.append('fullname', data._fullName);
        formData.append('email', data._email);
        formData.append('phone', data._phone);
        formData.append('password', data._password);
        */
        $.ajax({
            type: 'POST',
            //contentType: undefined,
            //processData: false,
            url: 'http://dh.api.local:8080/users',
            data: packet,
            //dataType: "json",
            beforeSend : function(xhr) {
                //xhr.setRequestHeader("Content-Type", "application/json");
                $('#loading').show();
            },
            error: function (request, status, error) {
                $('#loading').hide();
                var result = FathomMinds.FlowCore.FlowPacket.prototype.createFromJSON(request.responseText);
                console.log(request.responseText);
                console.log(result);
                // NOTIFICATION
                Dispatchers.Notification = new FathomMinds.FlowCore.FlowDispatcher({
                    'blocks':{
                        'Notificaion-Show': Flows.NotificationFlow.Show
                    },
                    'connections': []
                });

                Dispatchers.Notification.run([['Notificaion-Show', 'in', result]]);
            },
            success: function(request, status, error){
                $('#loading').hide();
                console.info(request, status, error);
            }
        });

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.JoinFormFlow.ProcessForm, FathomMinds.FlowCore.FlowBlock);
Flows.JoinFormFlow.ProcessForm.prototype._incomingPortNames = ['in'];
Flows.JoinFormFlow.ProcessForm.prototype._outgoingPortNames = [];

Dispatchers.JoinFlow = new FathomMinds.FlowCore.FlowDispatcher({
    'blocks':{
        'JoinFlow-01-CollectData': Flows.JoinFormFlow.CollectData,
        'JoinFlow-02-01-ValidateFullName': Flows.JoinFormFlow.ValidateFullName,
        'JoinFlow-02-02-ValidateEmail': Flows.JoinFormFlow.ValidateEmail,
        'JoinFlow-02-03-ValidateTerms': Flows.JoinFormFlow.ValidateTerms,
        'JoinFlow-03-CheckResults': Flows.JoinFormFlow.CheckResults,
        'JoinFlow-04-SetPassword': Flows.JoinFormFlow.SetPassword,
        'JoinFlow-05-ProcessForm': Flows.JoinFormFlow.ProcessForm,
        'JoinFlow-Error': Flows.JoinFormFlow.Error
    },
    'connections': [
        ['JoinFlow-01-CollectData', 'out-fullName', 'JoinFlow-02-01-ValidateFullName', 'in'],
        ['JoinFlow-02-01-ValidateFullName', 'out', 'JoinFlow-03-CheckResults', 'in-fullName'],
        ['JoinFlow-01-CollectData', 'out-email', 'JoinFlow-02-02-ValidateEmail', 'in'],
        ['JoinFlow-02-02-ValidateEmail', 'out', 'JoinFlow-03-CheckResults', 'in-email'],
        ['JoinFlow-01-CollectData', 'out-terms', 'JoinFlow-02-03-ValidateTerms', 'in'],
        ['JoinFlow-02-03-ValidateTerms', 'out', 'JoinFlow-03-CheckResults', 'in-terms'],
        ['JoinFlow-03-CheckResults', 'error', 'JoinFlow-Error', 'in'],
        ['JoinFlow-03-CheckResults', 'out', 'JoinFlow-04-SetPassword', 'in'],
        ['JoinFlow-04-SetPassword', 'back', 'JoinFlow-01-CollectData', 'in'],
        ['JoinFlow-04-SetPassword', 'out', 'JoinFlow-05-ProcessForm', 'in']
    ]
});
