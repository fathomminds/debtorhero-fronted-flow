Flows.ValidateEmailFlow = {};
Flows.ValidateEmailFlow.Validate = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var _data = _this.getIncomingPort('in').getPacket().getData();
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if(_data=='' || !_data || _data==null){
            var Error = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                ['Email can not be empty!'],
                null
            );
            _this.setOutgoingPort('error', Error);
            _this.sendPort(Dispatchers.ValidateEmail, 'error');
        } else if( !re.test(_data)){
            var Error = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                ['Invalid email address!'],
                null
            );
            _this.setOutgoingPort('error', Error);
            _this.sendPort(Dispatchers.ValidateEmail, 'error');
        } else {
            var requestPayload = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['email validation'],
                {"email": _this.getIncomingPort('in').getPacket().getData()}
            );
            requestPayload = requestPayload.jsonSerialize();
            $.ajax({
              type: "POST",
              url: DebtorHero.Config.ApiURL + DebtorHero.Config.Endpoints.checkEmail,
              data: requestPayload,
              error: function(rsp){
                  var Error = FathomMinds.FlowCore.FlowPacket.prototype.createFromJSON(rsp.responseText)
                  _this.setOutgoingPort('error', Error);
                  _this.sendPort(Dispatchers.ValidateEmail, 'error');
              },
              success: function(rsp){
                  var Success = new FathomMinds.FlowCore.FlowPacket(
                      FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                      ['Email validated: OK'],
                      {'email': _data}
                  );
                  _this.setOutgoingPort('out', Success);
                  _this.sendPort(Dispatchers.ValidateEmail, 'out');
              }
            });
        }

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidateEmailFlow.Validate, FathomMinds.FlowCore.FlowBlock);
Flows.ValidateEmailFlow.Validate.prototype._incomingPortNames = ['in'];
Flows.ValidateEmailFlow.Validate.prototype._outgoingPortNames = ['out', 'error'];

Flows.ValidateEmailFlow.Error = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var result = _this.getIncomingPort('in').getPacket();
        Dispatchers.ValidateEmail.setOutput(result);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidateEmailFlow.Error, FathomMinds.FlowCore.FlowBlock);
Flows.ValidateEmailFlow.Error.prototype._incomingPortNames = ['in'];
Flows.ValidateEmailFlow.Error.prototype._outgoingPortNames = [];

Flows.ValidateEmailFlow.Success = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var result = _this.getIncomingPort('in').getPacket();
        Dispatchers.ValidateEmail.setOutput(result);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidateEmailFlow.Success, FathomMinds.FlowCore.FlowBlock);
Flows.ValidateEmailFlow.Success.prototype._incomingPortNames = ['in'];
Flows.ValidateEmailFlow.Success.prototype._outgoingPortNames = [];

Dispatchers.ValidateEmail = new FathomMinds.FlowCore.FlowDispatcher({
    'blocks':{
        'Email-01-Validate': Flows.ValidateEmailFlow.Validate,
        'Email-02-01-Error': Flows.ValidateEmailFlow.Error,
        'Email-02-02-Success': Flows.ValidateEmailFlow.Success
    },
    'connections': [
        ['Email-01-Validate', 'error', 'Email-02-01-Error', 'in'],
        ['Email-01-Validate', 'out', 'Email-02-02-Success', 'in']
    ]
});
