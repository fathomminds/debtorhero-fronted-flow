Flows.InitPage = {};
Flows.InitPage.InitPage = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        for(var key in _this.getIncomingPort('in').getPacket().getData().Config){
            DebtorHero.Config[key] = _this.getIncomingPort('in').getPacket().getData().Config[key];
        }

        var packet = new FathomMinds.FlowCore.FlowPacket(
            FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
            ['Register up events'],
            null
        );
        _this.setOutgoingPort('out', packet);
        _this.sendPort(Dispatchers.Index, 'out');

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.InitPage.InitPage, FathomMinds.FlowCore.FlowBlock);
Flows.InitPage.InitPage.prototype._incomingPortNames = ['in'];
Flows.InitPage.InitPage.prototype._outgoingPortNames = ['out'];

Flows.InitPage.RegisterEvents = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var href = 'mai'; href += 'lt' + 'o:'; href += 'i'; href += 'nfo'; href += '@fa'; href += 'thommind' + 's'; href += '.co'; href += 'm';
        $('.contact-mnu').attr('href',href);

        var inputs = document.querySelectorAll( '.inputfile' );
        Array.prototype.forEach.call( inputs, function( input )
        {
            var label	 = input.nextElementSibling,
                labelVal = label.innerHTML;

            input.addEventListener( 'change', function( e )
            {
                var fileName = '';
                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName ) {
                    label.querySelector('span').innerHTML = fileName;
                }
                else {
                    label.querySelector('span').innerHTML = this.getAttribute( 'data-default-caption' );
                }
            });
        });

        var introBtnClickFunction = function(e){
            e.preventDefault();
            if(CurrentDevice=='phone'){
                $('html, body').animate({
                    scrollTop: $("#about").offset().top -20
                }, 600);
            } else if(CurrentDevice=='tablet'){
                $('html, body').animate({
                    scrollTop: $("#about").offset().top - 20
                }, 600);
            } else {
                $('html, body').animate({
                    scrollTop: $("#about").offset().top + 10
                }, 600);
            }
        };

        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('intro-btn-1'), 'click', introBtnClickFunction );
        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('intro-btn-2'), 'click', introBtnClickFunction );
        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('bounce'), 'click', introBtnClickFunction );

        var aboutMenuBtnClickFunction = function(e) {
            e.preventDefault();
            if(CurrentDevice=='phone'){
                $('html, body').animate({
                    scrollTop: $("#about").offset().top -20
                }, 600);
            } else if(CurrentDevice=='tablet'){
                $('html, body').animate({
                    scrollTop: $("#about").offset().top - 20
                }, 600);
            } else {
                $('html, body').animate({
                    scrollTop: $("#about").offset().top + 10
                }, 600);
            }
        };
        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('about-mnu'), 'click', aboutMenuBtnClickFunction );

        var menuHomeBtnClickFunction = function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, 600);
        };
        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('home-mnu'), 'click', menuHomeBtnClickFunction );

        var joinMenuButtonClickFunction = function(e) {
            e.preventDefault();
            if(CurrentDevice=='phone'){
                $('html, body').animate({
                    scrollTop: $("#careers").offset().top -20
                }, 600);
            } else if(CurrentDevice=='tablet'){
                $('html, body').animate({
                    scrollTop: $("#careers").offset().top - 20
                }, 600);
            } else {
                $('html, body').animate({
                    scrollTop: $("#careers").offset().top + 10
                }, 600);
            }
        };
        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('join-btn-1'), 'click', joinMenuButtonClickFunction );
        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('join-btn-2'), 'click', joinMenuButtonClickFunction );



        var inputEmailBlurFunction = function(){
            var email = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['Sending email for validation'],
                $('#career-email').val()
            );
            Dispatchers.ValidateEmail = new FathomMinds.FlowCore.FlowDispatcher({
                'blocks':{
                    'Email-01-Validate': Flows.ValidateEmailFlow.Validate,
                    'Email-02-01-Error': Flows.ValidateEmailFlow.Error,
                    'Email-02-02-Success': Flows.ValidateEmailFlow.Success
                },
                'connections': [
                    ['Email-01-Validate', 'error', 'Email-02-01-Error', 'in'],
                    ['Email-01-Validate', 'out', 'Email-02-02-Success', 'in']
                ]
            });
            Dispatchers.ValidateEmail.onOutput = function(){
                var result = this._output;
                if(result.getType()=='ERROR'){
                    $('#career-email').addClass('input-error');
                    // NOTIFICATION
                    Dispatchers.Notification = new FathomMinds.FlowCore.FlowDispatcher({
                        'blocks':{
                            'Notificaion-Show': Flows.NotificationFlow.Show
                        },
                        'connections': []
                    });
                    Dispatchers.Notification.run([['Notificaion-Show', 'in', result]]);
                }
            };
            Dispatchers.ValidateEmail.run([['Email-01-Validate', 'in', email]]);
        };
        Dispatchers.Index.registerEvent(
            _this.getId(),
            document.getElementById('career-email'),
            'blur',
            inputEmailBlurFunction
        );

        var inputFullNameBlurFunction = function(e){
            //Dispatchers.ValidateFullName.onOutput = function(){};
            Dispatchers.Index.removeRegisteredEvent(_this.getId(), document.getElementById('career-fullname'), 'focus', joinButtonClickFunction);
            Dispatchers.Index.removeRegisteredEvent(_this.getId(), document.getElementById('career-email'), 'focus', joinButtonClickFunction);
            Dispatchers.Index.removeRegisteredEvent(_this.getId(), document.getElementById('career-phone'), 'focus', joinButtonClickFunction);
            var fullName = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['Sending fullName for validation'],
                $('#career-fullname').val()
            );
            Dispatchers.ValidateFullName = new FathomMinds.FlowCore.FlowDispatcher({
                'blocks':{
                    'FullName-01-Validate': Flows.ValidateFullNameFlow.Validate,
                    'FullName-02-01-Error': Flows.ValidateFullNameFlow.Error,
                    'FullName-02-02-Success': Flows.ValidateFullNameFlow.Success
                },
                'connections': [
                    ['FullName-01-Validate', 'error', 'FullName-02-01-Error', 'in'],
                    ['FullName-01-Validate', 'out', 'FullName-02-02-Success', 'in']
                ]
            });
            Dispatchers.ValidateFullName.onOutput = function(){
                var result = this._output;
                if(result.getType()=='ERROR'){
                    $('#career-fullname').addClass('input-error');
                    // NOTIFICATION
                    Dispatchers.Notification = new FathomMinds.FlowCore.FlowDispatcher({
                        'blocks':{
                            'Notificaion-Show': Flows.NotificationFlow.Show
                        },
                        'connections': []
                    });
                    Dispatchers.Notification.run([['Notificaion-Show', 'in', result]]);
                }
            };
            Dispatchers.ValidateFullName.run([['FullName-01-Validate', 'in', fullName]]);
        };

        Dispatchers.Index.registerEvent(
            _this.getId(),
            document.getElementById('career-fullname'),
            'blur',
            inputFullNameBlurFunction
        );

        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('career-fullname'), 'focus', function(){ $('#career-fullname').removeClass('input-error') } );
        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('career-email'), 'focus', function(){ $('#career-email').removeClass('input-error') } );
        Dispatchers.Index.registerEvent( _this.getId(), document.getElementById('career-phone'), 'focus', function(){ $('#career-phone').removeClass('input-error') } );

        var joinButtonClickFunction = function(){
            var packet = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['Proceed with Join'],
                null
            );
            Dispatchers.JoinFlow.run([['JoinFlow-01-CollectData', 'in', packet]]);
        };

        var checkKeyUpForJoinFunction = function(e){ if(e.keyCode==13){ joinButtonClickFunction(); } };

        Dispatchers.Index.registerEvent(_this.getId(), document.getElementById('apply-btn'), 'click', joinButtonClickFunction);
        Dispatchers.Index.registerEvent(_this.getId(), document.getElementById('career-fullname'), 'keyup', checkKeyUpForJoinFunction);
        Dispatchers.Index.registerEvent(_this.getId(), document.getElementById('career-email'), 'keyup', checkKeyUpForJoinFunction);
        Dispatchers.Index.registerEvent(_this.getId(), document.getElementById('career-phone'), 'keyup', checkKeyUpForJoinFunction);


        // terms-open-btn
        var acceptTermsFunction = function(){
            $('#accept-terms').attr('checked', 'checked');
            closeTermsFunction();
        };

        var closeTermsFunctionOnEscape = function(e){ if(e.keyCode==27){closeTermsFunction();} };

        var closeTermsFunction = function(){
            $(document.body).removeClass('noscroll');
            $('#blackOverlay').hide();
            $('#terms-modal').fadeOut();
            Dispatchers.Index.removeRegisteredEvent(
                _this.getId(),
                document.getElementById('blackOverlay'),
                'click',
                closeTermsFunction
            );
            Dispatchers.Index.removeRegisteredEvent(
                _this.getId(),
                document.getElementById('application'),
                'keyup',
                closeTermsFunctionOnEscape
            );
        };

        var openTermsFunction = function(e){
            e.preventDefault();
            $(document.body).addClass('noscroll');
            $('#terms-modal').fadeIn();
            $('#blackOverlay').show();
            Dispatchers.Index.registerEvent(
                _this.getId(),
                document.getElementById('blackOverlay'),
                'click',
                closeTermsFunction
            );
            Dispatchers.Index.registerEvent(
                _this.getId(),
                document.getElementById('application'),
                'keyup',
                closeTermsFunctionOnEscape
            );
        };

        Dispatchers.Index.registerEvent(
            _this.getId(),
            document.getElementById('terms-open-btn'),
            'click',
            openTermsFunction
        );

        Dispatchers.JoinFlow.registerEvent(
            _this.getId(),
            document.getElementById('terms-accept-btn'),
            'click',
            acceptTermsFunction
        );
        Dispatchers.JoinFlow.registerEvent(
            _this.getId(),
            document.getElementById('terms-close-btn'),
            'click',
            closeTermsFunction
        );

        // SIGN IN
        var checkKeyUpSignInModal = function(e){
            if(e.keyCode==27) {closeSignInModal();}
            if(e.keyCode==13) {
                processSignIn();
            }
        };
        var openSignInModal = function(e){
            e.preventDefault();
            $(document.body).addClass('noscroll');
            $('#blackOverlay').show();
            $('#signin-modal').fadeIn();
            Dispatchers.Index.registerEvent(
                _this.getId(),
                document.getElementById('blackOverlay'),
                'click',
                closeSignInModal
            );
            if(CurrentDevice=='desktop'){
                Dispatchers.Index.registerEvent(
                    _this.getId(),
                    document.getElementById('application'),
                    'keyup',
                    checkKeyUpSignInModal
                );
            }
        };

        var closeSignInModal = function(){
            $(document.body).removeClass('noscroll');
            $('#blackOverlay').hide();
            $('#signin-modal').fadeOut();
            $('#signin-email').val('');
            $('#signin-password').val('');
            Dispatchers.Index.removeRegisteredEvent(
                _this.getId(),
                document.getElementById('application'),
                'keyup',
                checkKeyUpSignInModal
            );
            Dispatchers.Index.removeRegisteredEvent(
                _this.getId(),
                document.getElementById('blackOverlay'),
                'click',
                closeSignInModal
            );
        };

        var processSignIn = function(){

            Dispatchers.SignIn = new FathomMinds.FlowCore.FlowDispatcher({
                'blocks':{
                    'SignIn-01-Process': Flows.SignInFlow.Process,
                    'SignIn-02-01-Error': Flows.SignInFlow.Error,
                    'SignIn-02-02-Success': Flows.SignInFlow.Success
                },
                'connections': [
                    ['SignIn-01-Process', 'error', 'SignIn-02-01-Error', 'in'],
                    ['SignIn-01-Process', 'out', 'SignIn-02-02-Success', 'in']
                ]
            });

            var collectedData = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['check collected data'],
                {
                    "email": $('#signin-email').val(),
                    "password": $('#signin-password').val(),
                }
            );

            Dispatchers.SignIn.run([
                ['SignIn-01-Process', 'in', collectedData]
            ]);
        };

        Dispatchers.Index.registerEvent(
            _this.getId(),
            document.getElementById('signin-btn'),
            'click',
            openSignInModal
        );

        Dispatchers.Index.registerEvent(
            _this.getId(),
            document.getElementById('signin-close-btn'),
            'click',
            closeSignInModal
        );

        Dispatchers.Index.registerEvent(
            _this.getId(),
            document.getElementById('signin-submit-btn'),
            'click',
            processSignIn
        );

        /* Bounce */



        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.InitPage.RegisterEvents, FathomMinds.FlowCore.FlowBlock);
Flows.InitPage.RegisterEvents.prototype._incomingPortNames = ['in'];
Flows.InitPage.RegisterEvents.prototype._outgoingPortNames = ['out'];

Dispatchers.Index = new FathomMinds.FlowCore.FlowDispatcher({
    'blocks':{
        'InitPage-01-InitPage': Flows.InitPage.InitPage,
        'InitPage-02-RegisterEvents': Flows.InitPage.RegisterEvents
    },
    'connections': [
        ['InitPage-01-InitPage', 'out', 'InitPage-02-RegisterEvents', 'in']
    ]
});

var initPacket = new FathomMinds.FlowCore.FlowPacket(
    FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
    ['Page init packet'],
    {'Config': {
        'language': 'en'
    }}
);

Dispatchers.Index.run([
    ['InitPage-01-InitPage', 'in', initPacket]
]);
