Flows.ValidateFullNameFlow = {};
Flows.ValidateFullNameFlow.Validate = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var _data = _this.getIncomingPort('in').getPacket().getData();

        if(_data=='' || !_data || _data==null){
            var Error = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                ['Full name can not be empty!'],
                null
            );
            _this.setOutgoingPort('error', Error);
            _this.sendPort(Dispatchers.ValidateFullName, 'error');
        } else {
            var Success = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['FullName validated: OK'],
                {'fullname': _data}
            );
            _this.setOutgoingPort('out', Success);
            _this.sendPort(Dispatchers.ValidateFullName, 'out');
        }

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidateFullNameFlow.Validate, FathomMinds.FlowCore.FlowBlock);
Flows.ValidateFullNameFlow.Validate.prototype._incomingPortNames = ['in'];
Flows.ValidateFullNameFlow.Validate.prototype._outgoingPortNames = ['out', 'error'];

Flows.ValidateFullNameFlow.Error = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var result = _this.getIncomingPort('in').getPacket();
        Dispatchers.ValidateFullName.setOutput(result);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidateFullNameFlow.Error, FathomMinds.FlowCore.FlowBlock);
Flows.ValidateFullNameFlow.Error.prototype._incomingPortNames = ['in'];
Flows.ValidateFullNameFlow.Error.prototype._outgoingPortNames = [];

Flows.ValidateFullNameFlow.Success = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var result = _this.getIncomingPort('in').getPacket();
        Dispatchers.ValidateFullName.setOutput(result);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidateFullNameFlow.Success, FathomMinds.FlowCore.FlowBlock);
Flows.ValidateFullNameFlow.Success.prototype._incomingPortNames = ['in'];
Flows.ValidateFullNameFlow.Success.prototype._outgoingPortNames = [];

Dispatchers.ValidateFullName = new FathomMinds.FlowCore.FlowDispatcher({
    'blocks':{
        'FullName-01-Validate': Flows.ValidateFullNameFlow.Validate,
        'FullName-02-01-Error': Flows.ValidateFullNameFlow.Error,
        'FullName-02-02-Success': Flows.ValidateFullNameFlow.Success
    },
    'connections': [
        ['FullName-01-Validate', 'error', 'FullName-02-01-Error', 'in'],
        ['FullName-01-Validate', 'out', 'FullName-02-02-Success', 'in']
    ]
});
