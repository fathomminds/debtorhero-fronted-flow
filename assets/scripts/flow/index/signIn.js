Flows.SignInFlow = {};
Flows.SignInFlow.Process = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var collectedData = _this.getIncomingPort('in').getPacket();
        var email = collectedData.getData()['email'];
        var password = collectedData.getData()['password'];
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if(email=='' || !email || email==null){
            var Error = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                ['Email can not be empty!'],
                null
            );
            _this.setOutgoingPort('error', Error);
            _this.sendPort(Dispatchers.SignIn, 'error');
        } else if( !re.test(email)){
            var Error = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                ['Invalid email address!'],
                null
            );
            _this.setOutgoingPort('error', Error);
            _this.sendPort(Dispatchers.SignIn, 'error');
        } else if(password.length == 0){
            var Error = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                ['Password can not be empty!'],
                null
            );
            _this.setOutgoingPort('error', Error);
            _this.sendPort(Dispatchers.SignIn, 'error');
        } else {
            var requestPayload = _this.getIncomingPort('in').getPacket().jsonSerialize();
            $.ajax({
                type: 'POST',
                url: DebtorHero.Config.ApiURL + DebtorHero.Config.Endpoints.login,
                data: requestPayload,
                //dataType: "json",
                beforeSend : function(xhr) {
                    //xhr.setRequestHeader("Content-Type", "application/json");
                    $('#loading').show();
                },
                error: function(rsp){
                    $('#loading').hide();
                    var responsePacket = FathomMinds.FlowCore.FlowPacket.prototype.createFromJSON(rsp.responseText);
                    var Error = new FathomMinds.FlowCore.FlowPacket(
                        FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                        [responsePacket.getMessages()[0]],
                        null
                    );
                    _this.setOutgoingPort('error', Error);
                    _this.sendPort(Dispatchers.SignIn, 'error');
              },
              success: function(rsp){
                  var responsePacket = FathomMinds.FlowCore.FlowPacket.prototype.createFromObject(rsp);
                  localStorage.setItem('jwtToken', responsePacket.getData()['jwt']);
                  $('#loading').hide();
                  var Success = new FathomMinds.FlowCore.FlowPacket(
                      FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                      [responsePacket.getData()['message']],
                      null
                  );
                  _this.setOutgoingPort('out', Success);
                  _this.sendPort(Dispatchers.SignIn, 'out');
                  // Redirect the logged in user to the profile
                  //window.location.href = 'profile.php';
              }
            });
        }



        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.SignInFlow.Process, FathomMinds.FlowCore.FlowBlock);
Flows.SignInFlow.Process.prototype._incomingPortNames = ['in'];
Flows.SignInFlow.Process.prototype._outgoingPortNames = ['out', 'error'];

Flows.SignInFlow.Error = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var result = _this.getIncomingPort('in').getPacket();
        // NOTIFICATION
        Dispatchers.Notification = new FathomMinds.FlowCore.FlowDispatcher({
            'blocks':{
                'Notificaion-Show': Flows.NotificationFlow.Show
            },
            'connections': []
        });

        Dispatchers.Notification.run([['Notificaion-Show', 'in', result]]);
        Dispatchers.SignIn.setOutput(result);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.SignInFlow.Error, FathomMinds.FlowCore.FlowBlock);
Flows.SignInFlow.Error.prototype._incomingPortNames = ['in'];
Flows.SignInFlow.Error.prototype._outgoingPortNames = [];

Flows.SignInFlow.Success = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        $(document.body).removeClass('noscroll');
        $('#signin-modal').fadeOut();
        $('#blackOverlay').hide();
        $('#signin-email').val('');
        $('#signin-password').val('');

        var result = _this.getIncomingPort('in').getPacket();
        Dispatchers.SignIn.setOutput(result);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.SignInFlow.Success, FathomMinds.FlowCore.FlowBlock);
Flows.SignInFlow.Success.prototype._incomingPortNames = ['in'];
Flows.SignInFlow.Success.prototype._outgoingPortNames = [];
