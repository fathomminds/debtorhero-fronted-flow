Flows.ValidateTermsFlow = {};
Flows.ValidateTermsFlow.Validate = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var _data = _this.getIncomingPort('in').getPacket().getData();

        if(_data=='' || !_data || _data==null){
            var Error = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                ['Must accept Terms and Conditions!'],
                null
            );
            _this.setOutgoingPort('error', Error);
            _this.sendPort(Dispatchers.ValidateTerms, 'error');
        } else {
            var Success = new FathomMinds.FlowCore.FlowPacket(
                FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
                ['Terms validated: OK'],
                {'terms': _data}
            );
            _this.setOutgoingPort('out', Success);
            _this.sendPort(Dispatchers.ValidateTerms, 'out');
        }

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidateTermsFlow.Validate, FathomMinds.FlowCore.FlowBlock);
Flows.ValidateTermsFlow.Validate.prototype._incomingPortNames = ['in'];
Flows.ValidateTermsFlow.Validate.prototype._outgoingPortNames = ['out', 'error'];

Flows.ValidateTermsFlow.Error = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var result = _this.getIncomingPort('in').getPacket();
        Dispatchers.ValidateTerms.setOutput(result);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidateTermsFlow.Error, FathomMinds.FlowCore.FlowBlock);
Flows.ValidateTermsFlow.Error.prototype._incomingPortNames = ['in'];
Flows.ValidateTermsFlow.Error.prototype._outgoingPortNames = [];

Flows.ValidateTermsFlow.Success = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var result = _this.getIncomingPort('in').getPacket();
        Dispatchers.ValidateTerms.setOutput(result);


        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ValidateTermsFlow.Success, FathomMinds.FlowCore.FlowBlock);
Flows.ValidateTermsFlow.Success.prototype._incomingPortNames = ['in'];
Flows.ValidateTermsFlow.Success.prototype._outgoingPortNames = [];

Dispatchers.ValidateTerms = new FathomMinds.FlowCore.FlowDispatcher({
    'blocks':{
        'Terms-01-Validate': Flows.ValidateTermsFlow.Validate,
        'Terms-02-01-Error': Flows.ValidateTermsFlow.Error,
        'Terms-02-02-Success': Flows.ValidateTermsFlow.Success
    },
    'connections': [
        ['Terms-01-Validate', 'error', 'Terms-02-01-Error', 'in'],
        ['Terms-01-Validate', 'out', 'Terms-02-02-Success', 'in']
    ]
});
