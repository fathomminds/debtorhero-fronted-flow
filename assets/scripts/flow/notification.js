Flows.NotificationFlow = {};
Flows.NotificationFlow.Show = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        //console.log($('#notification').is(':visible')==false);
        if($('#notification').is(':visible')){
            var result = _this.getIncomingPort('in').getPacket();
            $('#notification').animate({
                height: '6rem'
            }, 400 , function(){
                $('#notification')
                    .removeClass('notification-success')
                    .removeClass('notification-error')
                    .removeClass('notification-info')
                    .addClass('notification-'+result.getType().toLowerCase());
                $('#notification').html(result.getMessages()[0]);
            });

            setTimeout(function(){
                $('#notification').animate({
                    height: 0
                }, 400 , function(){
                    $('#notification')
                        .removeClass('notification-success')
                        .removeClass('notification-error')
                        .removeClass('notification-info');
                    $('#notification').html('');
                });
            }, 5000);

            Dispatchers.Notification.setOutput(result);
        }

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.NotificationFlow.Show, FathomMinds.FlowCore.FlowBlock);
Flows.NotificationFlow.Show.prototype._incomingPortNames = ['in'];
Flows.NotificationFlow.Show.prototype._outgoingPortNames = ['out'];
