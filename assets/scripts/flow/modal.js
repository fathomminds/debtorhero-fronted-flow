Flows.ModalFlow = {};
Flows.ModalFlow.Show = function(id){
    this.execute = function(dispatcher){
        var _this = this;
        _this.initBlock(id);
        console.info(_this._id);
        /*********************** CODE START ***********************/

        var config = _this.getIncomingPort('in').getPacket();


        var nullPacket = new FathomMinds.FlowCore.FlowPacket(
            FathomMinds.FlowCore.FlowPacket.prototype.TYPE_SUCCESS,
            ['Modal finished'],
            null
            );
        Dispatchers.Modal.setOutput(result);

        /*********************** CODE END ***********************/
    };
};
FathomMinds.FlowCore.HelperFunctions.extend(Flows.ModalFlow.Show, FathomMinds.FlowCore.FlowBlock);
Flows.ModalFlow.Show.prototype._incomingPortNames = ['in'];
Flows.ModalFlow.Show.prototype._outgoingPortNames = ['out'];
