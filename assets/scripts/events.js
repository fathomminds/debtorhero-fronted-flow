$(document).ready(function(){
    $("#notification").on('click',function(e) {
        $('#notification').animate({
            height: 0
        },400, function(){
            $('#notification')
                .removeClass('notification-success')
                .removeClass('notification-error')
                .removeClass('notification-info');
            $('#notification').html('');
        });
    });

    $('#signin-btn').on('click',function(e){
        $('#signin-modal').addClass('modal-active');
        $('#signin-controls').css('height',$('#signin-modal').height());
        $('#signin-controls').css('width',$('#signin-modal').width());
        $('html').addClass('noscroll');
        return false;
    });

    $('#signin-modal').on('click',function(e){
        if(e.target==$('#signin-controls').get(0)) {
            $('#signin-modal').removeClass('modal-active');
            $('html').removeClass('noscroll');
            return false;
        }
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $('#signin-modal').removeClass('modal-active');
            $('#terms-modal').removeClass('modal-active');
            $('html').removeClass('noscroll');
        }
    });

    $(window).on('resize',function(e){
        $('#signin-controls').css('height',$('#signin-modal').height());
        $('#signin-controls').css('width',$('#signin-modal').width());
    })

    $("#signin-email").on('focus input',function(e){
        $(this).removeClass('input-signin-error');
    });

    $("#signin-password").on('focus input',function(e){
        $(this).removeClass('input-signin-error');
    });

    $("#career-fullname").on('focus input',function(e){
        $(this).removeClass('input-error');
    });

    $("#career-email").on('focus input',function(e){
        $(this).removeClass('input-error');
    });

    $("#career-phone").on('focus input',function(e){
        $(this).removeClass('input-error');
    });

    $("#password").on('focus input',function(e){
        $(this).removeClass('input-error');
    });

    $("#confirm-password").on('focus input',function(e){
        $(this).removeClass('input-error');
    });

    $("#apply-btn").on('click',function(e) {
        if ($(this).hasClass("executing")) return;
        $(document).focus();
        var flow = {
            blocks: [
                {
                    name: 'DisableButton',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        $("#ripple").css('display','inline-block');
                        $("#signuptext").css('display','none');
                        $("#apply-btn").addClass('executing',true);
                        $('#notification').html('Signing up...');
                        $('#notification')
                            .removeClass('notification-success')
                            .removeClass('notification-error')
                            .removeClass('notification-info')
                            .addClass('notification-info');
                        $('#notification').animate({
                            height: 80
                        },400);
                        this.send('message','DONE');
                    }
                },
                {
                    name: 'CheckTerms',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var check = $('#accept-terms').prop('checked');
                        if (!check) {
                            $('#notification').animate({
                                height: 0
                            },400,function(){
                                $('#notification')
                                    .removeClass('notification-success')
                                    .removeClass('notification-error')
                                    .removeClass('notification-info')
                                    .addClass('notification-error');
                                $('#notification').html('Ooops! Please accept the terms and conditions!');
                                $('#notification').animate({
                                    height: 80
                                },400);
                            });
                            this.send('error','DONE');
                        } else {
                            this.send('message','DONE');
                        }
                    }
                },
                {
                    name: 'EnableButton',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        window.setTimeout(function(){
                            $('#notification').animate({
                                height: 0
                            },400,function(){
                                $('#notification')
                                    .removeClass('notification-success')
                                    .removeClass('notification-error')
                                    .removeClass('notification-info');
                                $('#notification').html('');
                                $("#ripple").css('display','none');
                                $("#signuptext").css('display','');
                                $("#apply-btn").removeClass('executing');
                            });
                        },4000);
                        this.send('message','DONE');
                    }
                },
                {
                    name: 'CollectInputs',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var fd = new FormData();

                        //CREATE REQUEST BODY
                        var request_body = {
                            position: $('#career-position').val(),
                            fullname: $('#career-fullname').val(),
                            email: $('#career-email').val(),
                            phone: $('#career-phone').val()
                        }

                        //ADD NEW FILES
                        $('.inputfile').each(function($idx){
                            var $value = this;
                            if($value.value!='' && $value.value!=undefined){
                                inputNameFull = 'cv';
                                file = $value.files[0];
                                //console.log(file);
                                fd.append(inputNameFull, $value.files[0]);
                            }
                        });

                        //APPEND REQUEST BODY
                        fd.append('request_body', JSON.stringify(request_body));

                        this.send('message',fd);
                    }
                },
                {
                    name: 'DoSignup',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var $this = this;
                        var inData = this.getIncomingData('in');
                        $.ajax({
                            url: '/signup',
                            data: inData,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function(response){
                                if (typeof response !== 'object') {
                                    response = JSON.parse(response);
                                }
                                if (response.type && response.type=='success') {
                                    $this.send('message', response);
                                } else {
                                    $this.send('error', response);
                                }
                            },
                            error: function(response) {
                                var res = {
                                    type: 'error',
                                    messages: [
                                        response.status + ': ' + response.statusText
                                    ],
                                    data: response
                                }
                                $this.send('error',res);
                            }
                        });
                    }
                },
                {
                    name: 'DisplaySuccessMessage',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var inData = this.getIncomingData('in');
                        $('#uid').val(inData.data.uid);
                        $('#token').val(inData.data.token);
                        $('#notification').animate({
                            height: 0
                        },400,function(){
                            $('#notification')
                                .removeClass('notification-success')
                                .removeClass('notification-error')
                                .removeClass('notification-info')
                                .addClass('notification-success');
                            $('#notification').html(inData.messages[0]);
                            $('#notification').animate({
                                height: 80
                            },400);
                        });
                        this.send('message','DONE');
                    }
                },
                {
                    name: 'DisplayPasswordForm',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var inData = this.getIncomingData('in');
                        $('#signupform').fadeOut();
                        $('#passwordform').fadeIn();
                        this.send('message','DONE');
                    }
                },
                {
                    name: 'DisplayErrorMessage',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var inData = this.getIncomingData('in');
                        //Bad inputs
                        if (inData.data.badinputs) {
                            for (idx in inData.data.badinputs) {
                                switch (inData.data.badinputs[idx]) {
                                    case 'fullname':
                                        $('#career-fullname').addClass('input-error');
                                        break;
                                    case 'email':
                                        $('#career-email').addClass('input-error');
                                        break;
                                    case 'phone':
                                        $('#career-phone').addClass('input-error');
                                        break;
                                }
                            }
                        }

                        $('#notification').animate({
                            height: 0
                        },400,function(){
                            $('#notification')
                                .removeClass('notification-success')
                                .removeClass('notification-error')
                                .removeClass('notification-info')
                                .addClass('notification-error');
                            $('#notification').html(inData.messages[0]);
                            $('#notification').animate({
                                height: 80
                            },400);
                        });
                        this.send('message','DONE');
                    }
                }
            ],
            connections: [
                {
                    sourceBlockName: 'DisableButton',
                    sourceBlockPort: 'message',
                    targetBlockName: 'CheckTerms',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'CheckTerms',
                    sourceBlockPort: 'message',
                    targetBlockName: 'CollectInputs',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'CheckTerms',
                    sourceBlockPort: 'error',
                    targetBlockName: 'EnableButton',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'CollectInputs',
                    sourceBlockPort: 'message',
                    targetBlockName: 'DoSignup',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DoSignup',
                    sourceBlockPort: 'message',
                    targetBlockName: 'DisplaySuccessMessage',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DoSignup',
                    sourceBlockPort: 'error',
                    targetBlockName: 'DisplayErrorMessage',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DisplaySuccessMessage',
                    sourceBlockPort: 'message',
                    targetBlockName: 'DisplayPasswordForm',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DisplayErrorMessage',
                    sourceBlockPort: 'message',
                    targetBlockName: 'EnableButton',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DisplayPasswordForm',
                    sourceBlockPort: 'message',
                    targetBlockName: 'EnableButton',
                    targetBlockPort: 'in'
                }

            ]
        }
        var PROCESS = new FlowDispatcher(flow);
        PROCESS.getBlock('DisableButton').receive('in','DONE');
    });

    $("#setpassword-btn").on('click',function(e) {
        if ($(this).hasClass("executing")) return;
        $(document).focus();
        var flow = {
            blocks: [
                {
                    name: 'DisableButton',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        $("#ripple2").css('display','inline-block');
                        $("#setpasswordtext").css('display','none');
                        $("#setpassword-btn").addClass('executing',true);
                        $('#notification').html('Setting password...');
                        $('#notification')
                            .removeClass('notification-success')
                            .removeClass('notification-error')
                            .removeClass('notification-info')
                            .addClass('notification-info');
                        $('#notification').animate({
                            height: 80
                        },400);
                        this.send('message','DONE');
                    }
                },
                {
                    name: 'EnableButton',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        window.setTimeout(function(){
                            $('#notification').animate({
                                height: 0
                            },400,function(){
                                $('#notification')
                                    .removeClass('notification-success')
                                    .removeClass('notification-error')
                                    .removeClass('notification-info');
                                $('#notification').html('');
                                $("#ripple2").css('display','none');
                                $("#setpasswordtext").css('display','');
                                $("#setpassword-btn").removeClass('executing');
                            });
                        },4000);
                        this.send('message','DONE');
                    }
                },
                {
                    name: 'CollectInputs',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var fd = new FormData();

                        //CREATE REQUEST BODY
                        var request_body = {
                            uid: $('#uid').val(),
                            token: $('#token').val(),
                            password: $('#password').val(),
                            confirm_password: $('#confirm-password').val(),
                        }

                        //APPEND REQUEST BODY
                        fd.append('request_body', JSON.stringify(request_body));

                        this.send('message',fd);
                    }
                },
                {
                    name: 'DoPasswordSet',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var $this = this;
                        var inData = this.getIncomingData('in');
                        $.ajax({
                            url: '/setpassword',
                            data: inData,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function(response){
                                if (typeof response !== 'object') {
                                    response = JSON.parse(response);
                                }
                                if (response.type && response.type=='success') {
                                    $this.send('message', response);
                                } else {
                                    $this.send('error', response);
                                }
                            },
                            error: function(response) {
                                var res = {
                                    type: 'error',
                                    messages: [
                                        response.status + ': ' + response.statusText
                                    ],
                                    data: response
                                }
                                $this.send('error',res);
                            }
                        });
                    }
                },
                {
                    name: 'DisplaySuccessMessage',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var inData = this.getIncomingData('in');
                        $('#notification').animate({
                            height: 0
                        },400,function(){
                            $('#notification')
                                .removeClass('notification-success')
                                .removeClass('notification-error')
                                .removeClass('notification-info')
                                .addClass('notification-success');
                            $('#notification').html(inData.messages[0]);
                            $('#notification').animate({
                                height: 80
                            },400);
                        });
                        this.send('message','DONE');
                    }
                },
                {
                    name: 'DisplayProfilePage',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var inData = this.getIncomingData('in');

                        location.href='/profile';

                        this.send('message','DONE');
                    }
                },
                {
                    name: 'DisplayErrorMessage',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var inData = this.getIncomingData('in');
                        //Bad inputs
                        if (inData.data.badinputs) {
                            for (idx in inData.data.badinputs) {
                                switch (inData.data.badinputs[idx]) {
                                    case 'password':
                                        $('#password').addClass('input-error');
                                        break;
                                    case 'confirm_password':
                                        $('#confirm-password').addClass('input-error');
                                        break;
                                }
                            }
                        }

                        $('#notification').animate({
                            height: 0
                        },400,function(){
                            $('#notification')
                                .removeClass('notification-success')
                                .removeClass('notification-error')
                                .removeClass('notification-info')
                                .addClass('notification-error');
                            $('#notification').html(inData.messages[0]);
                            $('#notification').animate({
                                height: 80
                            },400);
                        });
                        this.send('message','DONE');
                    }
                }
            ],
            connections: [
                {
                    sourceBlockName: 'DisableButton',
                    sourceBlockPort: 'message',
                    targetBlockName: 'CollectInputs',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'CollectInputs',
                    sourceBlockPort: 'message',
                    targetBlockName: 'DoPasswordSet',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DoPasswordSet',
                    sourceBlockPort: 'message',
                    targetBlockName: 'DisplaySuccessMessage',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DoPasswordSet',
                    sourceBlockPort: 'error',
                    targetBlockName: 'DisplayErrorMessage',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DisplaySuccessMessage',
                    sourceBlockPort: 'message',
                    targetBlockName: 'DisplayProfilePage',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DisplayErrorMessage',
                    sourceBlockPort: 'message',
                    targetBlockName: 'EnableButton',
                    targetBlockPort: 'in'
                }
            ]
        }
        var PROCESS = new FlowDispatcher(flow);
        PROCESS.getBlock('DisableButton').receive('in','DONE');
    });

    $("#signin-submit-btn").on('click',function(e) {
        if ($(this).hasClass("executing")) return;
        $(this).blur();
        var flow = {
            blocks: [
                {
                    name: 'DisableButton',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        $("#signin-submit-btn").addClass('executing',true);
                        $('#notification').html('Signing in...');
                        $('#notification')
                            .removeClass('notification-success')
                            .removeClass('notification-error')
                            .removeClass('notification-info')
                            .addClass('notification-info');
                        $('#notification').animate({
                            height: 80
                        },400);
                        this.send('message','DONE');
                    }
                },
                {
                    name: 'EnableButton',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        window.setTimeout(function(){
                            $('#notification').animate({
                                height: 0
                            },400,function(){
                                $('#notification')
                                    .removeClass('notification-success')
                                    .removeClass('notification-error')
                                    .removeClass('notification-info');
                                $('#notification').html('');
                                $("#signin-submit-btn").removeClass('executing');
                            });
                        },4000);
                        this.send('message','DONE');
                    }
                },
                {
                    name: 'CollectInputs',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var fd = new FormData();

                        //CREATE REQUEST BODY
                        var request_body = {
                            email: $('#signin-email').val(),
                            password: $('#signin-password').val()
                        }

                        //APPEND REQUEST BODY
                        fd.append('request_body', JSON.stringify(request_body));

                        this.send('message',fd);
                    }
                },
                {
                    name: 'DoSignin',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var $this = this;
                        var inData = this.getIncomingData('in');
                        $.ajax({
                            url: '/signin',
                            data: inData,
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function(response){
                                if (typeof response !== 'object') {
                                    response = JSON.parse(response);
                                }
                                if (response.type && response.type=='success') {
                                    $this.send('message', response);
                                } else {
                                    $this.send('error', response);
                                }
                            },
                            error: function(response) {
                                var res = {
                                    type: 'error',
                                    messages: [
                                        response.status + ': ' + response.statusText
                                    ],
                                    data: response
                                }
                                $this.send('error',res);
                            }
                        });
                    }
                },
                {
                    name: 'SigninSuccess',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var inData = this.getIncomingData('in');
                        $('#notification').animate({
                            height: 0
                        },400,function(){
                            location.href='/profile';
                        });
                        this.send('message','DONE');
                    }
                },
                {
                    name: 'DisplayErrorMessage',
                    incomingPorts: ['in'],
                    outgoingPorts: ['message','error'],
                    execute: function () {
                        var inData = this.getIncomingData('in');
                        //Bad inputs
                        if (inData.data && inData.data.badinputs) {
                            for (idx in inData.data.badinputs) {
                                switch (inData.data.badinputs[idx]) {
                                    case 'email':
                                        $('#signin-email').addClass('input-signin-error');
                                        break;
                                    case 'password':
                                        $('#signin-password').addClass('input-signin-error');
                                        break;
                                }
                            }
                        }

                        $('#notification').animate({
                            height: 0
                        },400,function(){
                            $('#notification')
                                .removeClass('notification-success')
                                .removeClass('notification-error')
                                .removeClass('notification-info')
                                .addClass('notification-error');
                            $('#notification').html(inData.messages[0]);
                            $('#notification').animate({
                                height: 80
                            },400);
                        });
                        this.send('message','DONE');
                    }
                }
            ],
            connections: [
                {
                    sourceBlockName: 'DisableButton',
                    sourceBlockPort: 'message',
                    targetBlockName: 'CollectInputs',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'CollectInputs',
                    sourceBlockPort: 'message',
                    targetBlockName: 'DoSignin',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DoSignin',
                    sourceBlockPort: 'message',
                    targetBlockName: 'SigninSuccess',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DoSignin',
                    sourceBlockPort: 'error',
                    targetBlockName: 'DisplayErrorMessage',
                    targetBlockPort: 'in'
                },
                {
                    sourceBlockName: 'DisplayErrorMessage',
                    sourceBlockPort: 'message',
                    targetBlockName: 'EnableButton',
                    targetBlockPort: 'in'
                }
            ]
        }
        var PROCESS = new FlowDispatcher(flow);
        window.setTimeout(function(){
            PROCESS.getBlock('DisableButton').receive('in','DONE');
        },50);
    });

    $('#terms-btn').on('click',function(e){
        $('#terms-modal').addClass('modal-active');
        $('#terms-controls').css('height',$('#terms-modal').height());
        $('#terms-controls').css('width',$('#terms-modal').width());
        $('html').addClass('noscroll');
        return false;
    });

    $('#terms-open-btn').on('click',function(e){
        $('#terms-modal').addClass('modal-active');
        $('#terms-controls').css('height',$('#terms-modal').height());
        $('#terms-controls').css('width',$('#terms-modal').width());
        $('html').addClass('noscroll');
        return false;
    });

    $('#terms-modal').on('click',function(e){
        if(e.target==$('#terms-controls').get(0)) {
            $('#terms-modal').removeClass('modal-active');
            $('html').removeClass('noscroll');
            return false;
        }
    });

    $('#terms-close-btn').on('click',function(e){
        $('#terms-modal').removeClass('modal-active');
        $('html').removeClass('noscroll');
        return false;
    });
});