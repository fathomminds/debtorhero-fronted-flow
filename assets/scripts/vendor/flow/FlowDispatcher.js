/**
* @name FlowDispatcher
* @namespace FathmMinds#FlowCore#FlowDispatcher
* @constructor
* @param {object} config
* @param {mixed} messages
* @param {mixed} data
*/
FathomMinds.FlowCore.FlowDispatcher = function (config) {
    if (null === config) config = {};
    if (!config.blocks) config.blocks = [];
    if (!config.connections) config.connections = [];

    this._blocks = {};
    this._connections = [];
    this._output;

    this._registeredEvents = {};

    this._configErrors = [];

    //Adding block set in config to the dispatcher.
    for(var block in config.blocks){
        if(config.blocks.hasOwnProperty(block)){
            if(this.isValidBlock(config.blocks[block])){
                this.addBlock(config.blocks[block], block);
            } else {
                this._configErrors.push('Invalid class in config.blocks <'+ block._id +'>');
            }
        }
    }

    //Setting the dispatcher's connection from config
    for(connection in config.connections){
        var connectionError = false;
        if(!this.isValidBlockId(config.connections[connection][0])){
            connectionError = true;
            this._configErrors.push('Invalid block ID in config.connections: <'+ config.connections[connection][0] +'>');
        } else if(!this.isValidOutgoingPort(config.connections[connection][0], config.connections[connection][1])) {
            connectionError = true;
            this._configErrors.push('Invalid outgoing port in config.connections: <'+ config.connections[connection][0] +' :: '+ config.connections[connection][1] +'>');
        }

        if(!this.isValidBlockId(config.connections[connection][2])){
            connectionError = true;
            this._configErrors.push('Invalid block ID in config.connections: <'+ config.connections[connection][2]+'>')
        } else if(!this.isValidIncomingPort(config.connections[connection][2], config.connections[connection][3])){
            connectionError = true;
            this._configErrors.push('Invalid incoming port in config.connections: <'+ config.connections[connection][2] +' :: '+ config.connections[connection][3] +'>');
        }
        if(!connectionError){
            this.addConnection(
                config.connections[connection][0],
                config.connections[connection][1],
                config.connections[connection][2],
                config.connections[connection][3]
            );
        }

    }
}

FathomMinds.FlowCore.FlowDispatcher.prototype = {
    constructor: FathomMinds.FlowCore.FlowDispatcher,

    /**
     * Returns a value indicating whether the specified block is valid.
     * @param string $blockClass the class name of the block
     * @return bool whether the block is valid
     */
    isValidBlock: function(block){
        var valid = false;
        if(typeof block === 'function' ){
            valid = true;
        }
        return valid;
    },
    /**
    * Returns a value indicating whether the specified block is valid.
    * @name isValidBlockId
    * @param {string} blockId - the ID of the block
    * @return {bool} bool
    */
    isValidBlockId: function(blockId){
        var valid = true;
        if(!this._blocks.hasOwnProperty(blockId)){
            valid = false;
        }
        return valid;
    },

    /**
    * Returns a value indicating whether the specified outgoing port is valid for the specified block ID.
    * @name isValidOutgoingPort
    * @param {string} blockId - the ID of the block
    * @param {string} portName - the name of the port
    * @return bool
    */
    isValidOutgoingPort: function(blockId, portName){
        var valid = true;
        if(!this._blocks[blockId]._outgoingPorts.hasOwnProperty(portName)){
            valid = false;
        }
        return valid;
    },

    /**
    * Returns a value indicating whether the specified incoming port is valid for the specified block.
    * @name isValidIncomingPort
    * @param {string} blockId - the ID of the block
    * @param {string} portName
    * @return {bool} bool
    */
    isValidIncomingPort: function(blockId, portName){
        var valid = true;
        if(!this._blocks[blockId]._incomingPorts[portName] || !this._blocks[blockId]._incomingPorts[portName] instanceof FathomMinds.FlowCore.FlowPort){
            valid = false;
        }
        return valid;
    },

    /**
    * Sets the output to the specified value.
    * @name setOutput
    * @param {mixed} output
    */
    setOutput: function(output){
        this._output = output;
        this.removeRegisteredEvent();
        this.onOutput();
    },

    /**
    * Gets the output from the dispatcher.
    * @name getOutput
    * @return {mixed} output
    */
    getOutput: function(){
        return this._output;
    },

    /**
    * Adds a new FlowBlock.
    * @name addBlock
    * @param {string} blockClass
    * @param {string} blockId
    */
    addBlock: function(blockClass, blockId){
        var config = {_incomingPorts:{}, _outgoingPorts:{}, _class: blockClass};
        var incomingPortNames = blockClass.prototype.getIncomingPortNames();
        var outgoingPortNames = blockClass.prototype.getOutgoingPortNames();
        for(var incomingPortName in incomingPortNames){
            config._incomingPorts[incomingPortNames[incomingPortName]] = new FathomMinds.FlowCore.FlowPort();
        }
        for(var outgoingPortName in outgoingPortNames){
            config._outgoingPorts[outgoingPortNames[outgoingPortName]] = new FathomMinds.FlowCore.FlowPort();
        }
        this._blocks[blockId] = config;
    },

    /**
    * Returns a FlowBlock object with the specified ID.
    * @name getBlock
    * @param {string} blockId
    * @return {object} FlowBlock
    */
    getBlock: function (blockId) {
        if(this._blocks.hasOwnProperty(blockId)){
            var block = new this._blocks[blockId]._class(blockId);
            return block;
        }
    },
    /**
     * Assigns the specified source block's specified outgoing port to the specified target block's specified incoming port.
     * @param {string} sourceBlockId - the ID of the source block
     * @param {string} sourceBlockPort - the port name of the source block
     * @param {string} targetBlockId - the ID of the target block
     * @param {string} targetBlockPort - the port name of the target block
     */
    addConnection: function (sourceBlockId,sourceBlockPort,targetBlockId,targetBlockPort) {
        //console.log('//addConnection: ', sourceBlockId, sourceBlockPort, targetBlockId, targetBlockPort);
        if (!this._blocks[sourceBlockId] || !this._blocks[targetBlockId]) { return; }
        if (!this._connections[sourceBlockId]) this._connections[sourceBlockId] = {};
        if (!this._connections[sourceBlockId][sourceBlockPort]) this._connections[sourceBlockId][sourceBlockPort] = {};
        if (!this._connections[sourceBlockId][sourceBlockPort][targetBlockId]) this._connections[sourceBlockId][sourceBlockPort][targetBlockId] = {};
        this._connections[sourceBlockId][sourceBlockPort][targetBlockId][targetBlockPort] = true;
    },

    /**
     * Checks whether the specified packet is a valid FlowPacket.
     * @name isValidPacket
     * @param {FlowPacket} portPacket
     * @return {bool} bool
     */
    isValidPacket: function(portPacket){
        var valid = true;
        if(!(portPacket instanceof FathomMinds.FlowCore.FlowPacket)) {
            valid = false;
        }
        return valid;
    },

    /**
     * Executes the FlowBlock with the specified ID.
     * @name executeBlock
     * @param {string} targetBlockId
     */
    executeBlock: function(targetBlockId){
        if(!this._output){
            var targetBlock = this.getBlock(targetBlockId);
            for(var item in this._blocks[targetBlockId]._incomingPorts){
                if(this._blocks[targetBlockId]._incomingPorts.hasOwnProperty(item)){
                    var portPacket = this._blocks[targetBlockId]._incomingPorts[item].getPacket();
                    if(portPacket == null || this.isValidPacket(portPacket)){
                        targetBlock.setIncomingPort(item, portPacket);
                    } else {
                        this._output = new FathomMinds.FlowCore.FlowPacket(
                            FathomMinds.FlowCore.FlowPacket.prototype.TYPE_ERROR,
                            ["Incorrect incoming packet on port [{"+targetBlock+"}:{"+item+"}]"],
                            portPacket
                        )
                        break;
                    }
                }
            }

            if(!this._output){
                var _this = this;
                setTimeout(function(){
                    targetBlock.execute(_this);
                }, 0);

            }
        }
    },
    /**
     * Starts the dispatching process with the specified config.
     * @name run
     * @param {array} initConfig
     * @return {FlowPacket} FlowPacket
     */
     run: function(initConfig){
         //console.log('//run: ', initConfig);
         var targetBlockIds = [];
         for(var config in initConfig){
             var targetBlockId = initConfig[config][0];
             var targetBlockIncomingPortName = initConfig[config][1];
             var targetBlockIncomingPortPacket = initConfig[config][2];
             if(this._blocks[targetBlockId]._incomingPorts[targetBlockIncomingPortName] != null){
                 this._blocks[targetBlockId]._incomingPorts[targetBlockIncomingPortName].set(targetBlockIncomingPortPacket);
                 targetBlockIds.push(targetBlockId);
             }
         }
         for(var i in targetBlockIds){
             if(this._blocks[targetBlockIds[i]]._class.prototype.isBlockReady(this._blocks[targetBlockIds[i]]._incomingPorts)){
                 this.executeBlock(targetBlockIds[i]);
             }
         }
         return this._output;
     },
     /**
      * @name dispatch
      * @param {string} sourceBlockId - the ID of the source block
      * @param {string} sourceBlockOutgoingPortName - the outgoing port name of the source block
      * @param {string} targetBlockIncomingPortPacket - the incoming port packet of the target block
      */
    dispatch: function (sourceBlockId, sourceBlockOutgoingPortName, targetBlockIncomingPortPacket) {
        //console.info('//dispatch',sourceBlockId, sourceBlockOutgoingPortName, targetBlockIncomingPortPacket);
        var targetBlockIds = [];
        if(this._connections[sourceBlockId][sourceBlockOutgoingPortName]){
            for(var targetBlockId in this._connections[sourceBlockId][sourceBlockOutgoingPortName]){
                for(var targetBlockIncomingPortName in this._connections[sourceBlockId][sourceBlockOutgoingPortName][targetBlockId]){
                    this._blocks[targetBlockId]._incomingPorts[targetBlockIncomingPortName].set(targetBlockIncomingPortPacket);
                    targetBlockIds.push(targetBlockId);
                }
            }
        }
        for(var i in targetBlockIds){
            if(this._blocks[targetBlockIds[i]]._class.prototype.isBlockReady(this._blocks[targetBlockIds[i]]._incomingPorts)){
                this.executeBlock(targetBlockIds[i]);
            }
        }
    },
    /**
    * Called when dispatcher.setOutput sets dispatcher._output.
    * Can be overriten in block definition for custom behaviour.
    */
    onOutput: function(){},

    /**
    * Registeres an event listener to the target DOM Object and collects all registered events to dispatcher._registeredEvents.
    * @param {string} blockId - The block's ID where the event is registered.
    * @param {object} DomOBJ - The target DOM Object.
    * @param {string} eventName - Name of the event to add listener to.
    * @param {function} userFunction - The function to register to the event.
    * @param {boolean} override - If set to TRUE, all other events with the eventName registered to this DOM Object will be removed [OPTIONAL].
    */
    registerEvent: function(blockId, DomOBJ, eventName, userFunction, override){
        if(override === true) this.removeRegisteredEvent(blockId, DomOBJ, eventName);
        DomOBJ.addEventListener(eventName, userFunction);
        if(typeof this._registeredEvents[blockId] !== 'object') this._registeredEvents[blockId] = {};
        if(typeof this._registeredEvents[blockId][eventName] !== 'object') this._registeredEvents[blockId][eventName] = [];
        this._registeredEvents[blockId][eventName].push({'DomOBJ': DomOBJ, 'userFunction': userFunction});
    },

    /**
    * Remove registered events and references in dispatcher._registeredEvents.
    * Usage 1: (blockId, DomOBJ, eventName, UserFunction) only the userFunction will be removed from the DomOBJ's eventName event in the specified blockId.
    * Usage 2: (blockId, DomOBJ, eventName) All registered userFunction will be removed from the DomOBJ's eventName event in the specified blockId.
    * Usage 3: (blockId, DomOBJ) All registered eventName events will be removed from the DomOBJ in the specified blockId.
    * Usage 4: (blockId) All DomOBJ objects will be removed with all registered eventName events from the specified blockId.
    * Usage 5: () All registered eventName events will be removed from all DomOBJ objects.
    * @param {string} blockId - The block's ID where the event is registered [OPTIONAL].
    * @param {object} DomOBJ - The target DOM Object [OPTIONAL].
    * @param {string} eventName - Name of the registered event [OPTIONAL].
    * @param {function} userFunction - The function registered to the event [OPTIONAL].
    */
    removeRegisteredEvent: function(blockId, DomOBJ, eventName, userFunction){
        if(blockId && DomOBJ && eventName && userFunction){
            var  i = this._registeredEvents[blockId][eventName].length;
            while(i--){
                if(
                    this._registeredEvents[blockId][eventName][i].DomOBJ == DomOBJ &&
                    this._registeredEvents[blockId][eventName][i].userFunction == userFunction
                ){
                    DomOBJ.removeEventListener(eventName, userFunction);
                    this._registeredEvents[blockId][eventName].splice(i, 1);
                }
            }
            if(this._registeredEvents[blockId][eventName].length==0){
                delete this._registeredEvents[blockId][eventName];
                if(this._registeredEvents[blockId].length == 0){
                    delete this._registeredEvents[blockId];
                }
            }
        } else if(blockId && DomOBJ && eventName){
            var  i = this._registeredEvents[blockId][eventName].length;
            while(i--){
                if(
                    this._registeredEvents[blockId][eventName][i].DomOBJ == DomOBJ
                ){
                    DomOBJ.removeEventListener(eventName, this._registeredEvents[blockId][eventName][i].userFunction);
                    this._registeredEvents[blockId][eventName].splice(i, 1);
                }
            }
            if(this._registeredEvents[blockId][eventName].length==0){
                delete this._registeredEvents[blockId][eventName];
                if(this._registeredEvents[blockId].length == 0){
                    delete this._registeredEvents[blockId];
                }
            }
        } else if(blockId && DomOBJ){
            for(var eventName in this._registeredEvents[blockId]){
                var i = this._registeredEvents[blockId][eventName].length;
                while(i--){
                    if(
                        this._registeredEvents[blockId][eventName][i].DomOBJ == DomOBJ
                    ){
                        DomOBJ.removeEventListener(eventName, this._registeredEvents[blockId][eventName][i].userFunction);
                        this._registeredEvents[blockId][eventName].splice(i, 1);
                    }
                }
                if(this._registeredEvents[blockId][eventName].length==0){
                    delete this._registeredEvents[blockId][eventName];
                    if(this._registeredEvents[blockId].length == 0){
                        delete this._registeredEvents[blockId];
                    }
                }
            }
        } else if(blockId){
            for(var eventName in this._registeredEvents[blockId]){
                var i = this._registeredEvents[blockId][eventName].length;
                while(i--){
                    this._registeredEvents[blockId][eventName][i].DomOBJ.removeEventListener(eventName, this._registeredEvents[blockId][eventName][i].userFunction);
                    this._registeredEvents[blockId][eventName].splice(i, 1);
                }
                this._registeredEvents[blockId] = {};
            }
        } else {
            for(var blockId in this._registeredEvents){
                for(var eventName in this._registeredEvents[blockId]){
                    var i = this._registeredEvents[blockId][eventName].length;
                    while(i--){
                        this._registeredEvents[blockId][eventName][i].DomOBJ.removeEventListener(eventName, this._registeredEvents[blockId][eventName][i].userFunction);
                        this._registeredEvents[blockId][eventName].splice(i, 1);
                    }
                    this._registeredEvents = {};
                }
            }
        }
    }
};
