var FathomMinds = {};
FathomMinds.FlowCore = {};
FathomMinds.FlowCore.HelperFunctions = {};

/** Extends class
* @param {object} ChildClass
* @param {object} ParentClass
*/
FathomMinds.FlowCore.HelperFunctions.extend = function(ChildClass, ParentClass) {
    ChildClass.prototype = Object.create(ParentClass.prototype);
	ChildClass.prototype.constructor = ChildClass;
};
