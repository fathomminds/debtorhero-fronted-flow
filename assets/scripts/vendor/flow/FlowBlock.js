 /**
 * @name FlowBlock
 * @namespace FathmMinds#FlowCore#FlowBlock
 * @constructor
 * @param {string} ID - config.id: name of the block
 */
FathomMinds.FlowCore.FlowBlock = function (id) {
    this._id = id;
    this._incomingPortNames = [];
    this._outgoingPortNames = [];

    this._incomingPorts = {};
    this._outgoingPorts = {};
};

FathomMinds.FlowCore.FlowBlock.prototype = {
    constructor: FathomMinds.FlowCore.FlowBlock,

    initBlock: function(id){
        this._id = id;
    },
    /**
    * Returns an array of the outgoing port names.
    * @return {array} array - the outgoing port names
    */
    getOutgoingPortNames: function(){
        return this._outgoingPortNames;
    },
    /**
    * Returns a value indicating whether the block has an outgoing port with the specified name.
    * @param {string} portName - the name of the port
    * @return {boolean} boolean
    */
    hasOutgoingPortName: function (portName) {
        return this._outgoingPorts.hasOwnProperty(portName);
    },
    /**
    * Returns an array of the incoming port names.
    * @return {array} array - the incoming port names
    */
    getIncomingPortNames: function(){
        return this._incomingPortNames;
    },
    /**
     * Returns a value indicating whether the block has an incoming port with the specified name.
     * @param {string} portName - the name of the port
     * @return {boolean} boolean
     */
    hasIncomingPortName: function (portName) {
        return this._incomingPorts.hasOwnProperty(portName);
    },
    /**
    * Returns the incoming data on the named port
    * @param {string} portName - name of the incoming port
    * @return {object} data - the data on the named incoming port
    */
    getIncomingData: function (portName) {
        var ret = null;
        if (this._incomingPorts.hasOwnProperty(portName)) {
            ret = this._incomingPorts[portName].getPacket().clonePacket();
        }
        return ret;
    },
    /**
    * Returns the outgoing data on the named port
    * @param {string} portName - name of the outgoing port
    * @return {object} data - the data on the named outgoing port
    */
    getOutgoingData: function (portName) {
        var ret = null;
        if (this._outgoingPorts.hasOwnProperty(portName)) {
            ret = this._outgoingPorts[portName].getPacket().clonePacket();
        }
        return ret;
    },

    /**
     * Returns the ID of the block.
     * @return {mixed} mixed - null|string
     */
    getId: function(){
        return this._id;
    },
    /**
    * Assigns the specified packet to the specified incoming port.
    * @param {string} portName
    * @param {FlowPacket} packet
    */
    setIncomingPort: function(portName, packet){
        if(!this._incomingPorts || typeof this._incomingPorts!=='object'){
            this._incomingPorts = {};
        }
        if(!this._incomingPorts.hasOwnProperty(portName)){
            this._incomingPorts[portName] = new FathomMinds.FlowCore.FlowPort();
        }
        this._incomingPorts[portName].set(packet);
    },
    /**
    * Returns the incoming port with the specified name.
    * @param {string} portName
    * @return {FlowPort} FlowPort
    */
    getIncomingPort: function(portName){
        var port = null;
        if(this._incomingPorts[portName]){
            port = this._incomingPorts[portName];
        }
        return port;
    },

    /**
    * Assigns the specified packet to the specified outgoing port.
    * @param {string} portName
    * @param {FlowPacket} packet
    */
    setOutgoingPort: function(portName, packet){
        if(!this._outgoingPorts || typeof this._outgoingPorts!=='object'){
            this._outgoingPorts = {};
        }
        if(!this._outgoingPorts.hasOwnProperty(portName)){
            this._outgoingPorts[portName] = new FathomMinds.FlowCore.FlowPort();
        }
        this._outgoingPorts[portName].set(packet);
    },
    /**
    * Returns the outgoing port with the specified name.
    * @param {string} portName
    * @return {FlowPort} FlowPort
    */
    getOutgoingPort: function(portName){
        var port = null;
        if(this._outgoingPorts[portName]){
            port = this._outgoingPorts[portName];
        }
        return port;
    },

    /**
    * Sends the packet to the target port
    * @param {FlowDispatcher} dispatcher
    * @param {string} outgoingPortName
    * @param {boolean} clonePacket - optional, if set to "true" the original packet will be cloned and sent to the named port.
    */
    sendPort: function(dispatcher, outgoingPortName, clonePacket){
        if(clonePacket === null || !clonePacket || clonePacket === undefined){
            var packet = this._outgoingPorts[outgoingPortName].getPacket();
        } else if(clonePacket === true && typeof clonePacket === 'boolean'){
            var originalPacket = this._outgoingPorts[outgoingPortName].getPacket();
            var packet = originalPacket.clonePacket();
        } else {
            throw new Error('Invalid parameter type: clonePacket');
            return;
        }
        dispatcher.dispatch(this._id, outgoingPortName, packet);
    },

    /**
    * Checks if the targetblock's incoming port(s) is/are ready
    * Can be overriten in application level, when the block is defined for custom behaviour.
    * @param {array} array - incoming ports of the block
    * @return {boolean} boolean
    */
    isBlockReady: function(incomingPorts){
        var targetBlockIsReady = true;
        for(var item in incomingPorts){
            if(!incomingPorts[item].isReady()){
                targetBlockIsReady = false;
            }
        }
        return targetBlockIsReady;
    },

    execute: function (dispatcher) {
        //DEFINE IN THE CREATED BLOCKS
    }
};
