/**
* @name FlowPort
* @namespace FathmMinds#FlowCore#FlowPort
* @constructor
*/
FathomMinds.FlowCore.FlowPort = function() {
    this._ready = false;
    this._packet = null;
};

FathomMinds.FlowCore.FlowPort.prototype = {
    constructor: FathomMinds.FlowCore.FlowPort,
    /**
    * Assigns the specified packet to the current port.
    * @name setPacket
    * @param {mixed} packet - to assign to the port
    */
    set: function(packet){
        this._ready = true;
        this._packet = packet;
    },
    /**
    * Returns whether the port is ready (if it has already been set).
    * @name isReady
    * @return {boolean} boolean - whether the port is ready
    */
    isReady: function(){
        return this._ready;
    },
    /**
    * Returns the packet data of the current port.
    * @name getPacket
    * @return {object} object - FlowPacket
    */
    getPacket: function(){
        return this._packet;
    },
};
