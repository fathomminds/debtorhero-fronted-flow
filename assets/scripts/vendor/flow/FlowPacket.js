/**
* @name FlowPacket
* @namespace FathmMinds#FlowCore#FlowPacket
* @constructor
* @param {string} type - type string of the packet
* @param {array} messages - array of messages
* @param {object} data - data object
*/
FathomMinds.FlowCore.FlowPacket = function(type, messages, data) {
    this._type = null;
    this._messages = [];
    this._data = {};

    if(!(type === null && messages === null && data === null)){
        this._type = type;
        this._messages = messages;
        this._data = data;
    }
};

FathomMinds.FlowCore.FlowPacket.prototype = {
    constructor: FathomMinds.FlowCore.FlowPacket,

    TYPE_ERROR: 'ERROR',
    TYPE_WARNING: 'WARNING',
    TYPE_SUCCESS: 'SUCCESS',


    /**
    * Sets the type of the packet. Can be "SUCCESS", "WARNING" or "ERROR".
    * @name setType
    * @param {string} strType - type string
    */
    setType: function (strType) {
        this._type = strType;
    },
    /**
    * Returns the type of the packet.
    * @name getType
    * @return {string} string - type string of the packet
    */
    getType: function(){
        return this._type;
    },
    /**
    * Adds message(s) to the current packet.
    * @name addMessage
    * @param {string} strMessage - the message to add
    */
    addMessage: function (strMessage) {
        this._messages.push(strMessage);
    },
    /**
    * Clears all mesages of the current packet
    * @name clearMessages
    **/
    clearMessages: function(){
        this._messages = [];
    },
    /**
    * Returns all messages of the current packet.
    * @name getMessages
    * @return {array} array - the messages of the packet
    */
    getMessages: function(){
        return this._messages;
    },
    /**
    * Returns all data of the current packet.
    * @name getData
    * @return {array} array - data of the packet
    */
    getData: function() {
        return this._data;
    },
    /**
    * Sets the data for the packet.
    * @name setData
    * @param {mixed} objData - the new data
    */
    setData: function(objData) {
        this._data = objData;
    },
    /**
    * Adds a new data item to the already existing data of the packet. [DEPRICATED]
    * @name addData
    * @param {object} data - the new data to add
    */
    addData: function(data){
        for(var key in data){
            this._data[key] = data[key];
        }
    },
    /**
    * Removes the data with the specified key.
    * @name removeData
    * @param {string} key
    */
    removeData: function(key){
        if (key > -1) {
            this._data.splice(key, 1);
        }
    },
    /**
    * Clears all data of the current packet.
    * @name clearData
    */
    clearData: function(){
        this._data = [];
    },
    /**
    * Returns a JSON encoded string version of the packet.
    * @return {string} JSON
    */
    jsonSerialize: function(){
        return JSON.stringify(this);
    },
	/**
     * Creates a FlowPacket from a JSON string
     * @param {string} JSON_STRING - encoded string from packet object
     * @return {object} FlowPacket
     */
    createFromJSON: function(jsonStr){
        var jsonObj = JSON.parse(jsonStr);
        return this.createFromObject(jsonObj);
    },
	/**
     * Creates a FlowPacket from an Object
     * @param {object} an object that can be transformed into a FlowPacket
     * @return {object} FlowPacket
     */
	createFromObject: function(stdObj){
		var obj = new FathomMinds.FlowCore.FlowPacket();
        obj._type = stdObj._type;
        obj._messages = stdObj._messages;
        obj._data = stdObj._data;
		return obj;
	},
    /** Clones the packet into a new instance */
    clonePacket: function() {
        return FathomMinds.FlowCore.FlowPacket.prototype.createFromJSON(this.jsonSerialize());
    }
};
