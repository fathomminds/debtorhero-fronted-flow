var JWT = {
    Config: {
        requestTokenUrl: 'dummy-feeds/request-token.php',
        requestTokenAction: {'action':'getToken'}
    }
};

JWT.interceptor = function(){
    this.authHeader = 'Authorization';
    this.authPrefix = 'Bearer ';
    this.tokenGetter = function() {
        return null;
    }

    var config = this;

    this.$get = ["$q", "$injector", "$rootScope", function ($q, $injector, $rootScope) {
        return {
            request: function (request) {
                if (request.skipAuthorization) {
                    return request;
                }

                request.headers = request.headers || {};
                // Already has an Authorization header
                if (request.headers[config.authHeader]) {
                    return request;
                }

                var tokenPromise = $q.when($injector.invoke(config.tokenGetter, this, {
                    config: request
                }));

                return tokenPromise.then(function(token) {
                    if (token) {
                        request.headers[config.authHeader] = config.authPrefix + token;
                    }
                    return request;
                });
            },
            responseError: function (response) {
                // handle the case where the user is not authenticated
                if (response.status === 401) {
                    $rootScope.$broadcast('unauthenticated', response);
                }
                return $q.reject(response);
            }
        };
    }];
};

JWT.urlBase64Decode = function(str) {
    var output = str.replace('-', '+').replace('_', '/');
    switch (output.length % 4) {
        case 0: { break; }
        case 2: { output += '=='; break; }
        case 3: { output += '='; break; }
        default: {
            throw 'Illegal base64url string!';
        }
    }
    return window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
}


JWT.decodeToken = function(token) {
    var parts = token.split('.');

    if (parts.length !== 3) {
        throw new Error('JWT must have 3 parts');
    }

    var decoded = JWT.urlBase64Decode(parts[1]);
    if (!decoded) {
        throw new Error('Cannot decode the token');
    }

    return JSON.parse(decoded);
}

JWT.getTokenExpirationDate = function(token) {
    var decoded;
    decoded = JWT.decodeToken(token);

    if(!decoded.exp) {
        return null;
    }

    var d = new Date(0); // The 0 here is the key, which sets the date to the epoch
    d.setUTCSeconds(decoded.exp);

    return d;
};

JWT.isTokenExpired = function(token) {
    var d = JWT.getTokenExpirationDate(token);

    if (!d) {
        return false;
    }

    // Token expired?
    return !(d.valueOf() > new Date().valueOf());
};


/** Sample request **/
/*
$.ajax({
    url: _targetEndpointURL,
    dataType : 'json',
    type: 'GET',
    headers: {"Authorization": 'Bearer ' + localStorage.getItem('jwtToken')}
    beforeSend : function(xhr) {
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json");
    },
    error : function() {
        // error handler
    },
    success: function(data) {
        console.log(data);
        return data;
    }
});
*/
