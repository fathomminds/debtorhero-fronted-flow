var Dispatchers = {};
var Flows = {};

var DebtorHero = {
    Config: {
        // REMOTEURL:  http://dh.fathomminds.com/
        // DEVURL:  http://dh.api.local:8080/
        ApiURL: 'http://dh.fathomminds.com/',
        Endpoints: {
            login: 'site/login',
            checkEmail: 'site/email-taken',
            users: 'users', // Type: POST -> data: flowPacket -> register user
        }
    }
};



$(document).ready(function(){

    $(document).scroll(function() {
        if ( $(this).scrollTop()>0 ) {
            $('#header').addClass('header-white');
        } else {
            $('#header').removeClass('header-white');
        }

        if ( $(this).scrollTop()>570 && $(this).scrollTop()<1260 ) {
            $('.bounce').fadeIn();
        } else {
            $('.bounce').fadeOut();
        }
    });

    setTimeout(function(){
        var videoHeight = $('video').height();
        $('#overlay').height(videoHeight);
    },100);
    $('video').get()[0].addEventListener('loadeddata', function() {
        var videoHeight = $('video').height();
        $('#overlay').height(videoHeight);
    }, false);
    $(window).resize(function(){
        var videoHeight = $('video').height();
        $('#overlay').height(videoHeight);
    });

});
