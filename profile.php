<?php
ob_start();
require 'Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$class = "desktop";
if($deviceType=='tablet'){
	$class = "tablet";
} else if($deviceType=='phone'){
    $class = "phone";
}
?>
<html>
<head>
    <title>DebtorHero - Profile</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- FLOW CORE SCRIPTS -->
    <script type="text/javascript" src="assets/scripts/vendor/flow/FlowCore.js"></script>
    <script type="text/javascript" src="assets/scripts/vendor/flow/FlowBlock.js"></script>
    <script type="text/javascript" src="assets/scripts/vendor/flow/FlowPacket.js"></script>
    <script type="text/javascript" src="assets/scripts/vendor/flow/FlowPort.js"></script>
    <script type="text/javascript" src="assets/scripts/vendor/flow/FlowDispatcher.js"></script>

    <!-- VENDOR SCRIPTS -->
    <script type="text/javascript" src="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/scripts/vendor/jquery/jquery-3.1.0.min.js"></script>

    <!-- STYLESHEETS -->
    <link rel="stylesheet" href="assets/css/style.css" media="screen"/>

    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="57x57" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://s3-eu-west-1.amazonaws.com/fm-debtorhero/assets/images/favicon/favicon-16x16.png">

    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Hind' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="assets/scripts/app.js"></script>
    <script type="text/javascript">var CurrentDevice = '<?=$class?>';</script>
</head>
<body class="<?=$class?>" id="application">

<div id="modal" class="modal-window">
    <div class="modal-content">
        <div class="modal-header"><h1>Modal title</h1></div>
        <div class="modal-text" id="terms-text">
            <p>Modal content</p>
        </div>
        <div class="modal-controls">
            <button id="modal-close-btn">Cancel</button>
        </div>
    </div>
</div>
<div id="blackOverlay"></div>
<div id="notification"></div>
<div id="loading"></div>
<script type="text/javascript" src="assets/scripts/flow/notification.js"></script>
</body>
</html>
